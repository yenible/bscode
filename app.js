/*
 * @Author: yenible
 * @Date: 2020-12-12 14:36:42
 * @LastEditors: yenible
 * @LastEditTime: 2021-04-11 17:06:22
 * @Description: xxx
 */
// 引入koa和router
const koa = require('koa');
const Router = require('koa-router');
const config = require('./config/my_config');
const bodyParser = require('koa-bodyparser');
const cors = require('koa2-cors');
const jwt = require('jsonwebtoken'); //token生成插件
const path = require('path')
var http = require('http');


// 实例化koa
const app = new koa();
const router = new Router();



// 引入users.js
const users = require("./routes/api/users");
const article = require("./routes/api/article");
const file = require("./routes/api/file");
const comment = require("./routes/api/comment");
const search = require("./routes/api/search");
const admin = require("./routes/api/admin");
const message = require("./routes/api/message");
const apiChat = require("./routes/api/apiChat");

// socket连接操作
const serve = require('koa-static');
// 1.主页静态网页 把静态页统一放到public中管理
const public = serve(path.join(__dirname) + '/public/');
app.use(public)

app.use(cors({
  origin: ['http://localhost:8080'],
  credentials: true //证书
}))
// app.use(ctx)
// 使用bodyParser
app.use(bodyParser({
  enableTypes: ['json', 'form', 'text'],
  formLimit: "3mb",
  queryString: {
    parameterLimit: 100000000000000
  }
}))


// router.use("/api/users",users)
// // 处理request method 为 option的问题 + 跨域中间件...
app.use(async (ctx, next) => {
  ctx.set('Access-Control-Allow-Origin', '*');
  ctx.set('Access-Control-Allow-Headers', 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With , yourHeaderFeild');
  ctx.set('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
  ctx.set('Access-Control-Allow-Credentials', 'true');
  // ctx.set('Access-Control-Allow-Headers', 'Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type, X-E4M-With');
  ctx.set('Content-Type', 'text/html; charset=utf-8');
  if (ctx.method == 'OPTIONS') {
    ctx.body = 200;
  } else {
    await next();
  }
});


// 配置路由地址,访问api/users的时候直接调用文件接口
router.use("/api/users", users);
router.use("/api/article", article);
router.use("/api/file", file)
router.use("/api/comment", comment)
router.use("/api/search", search)
router.use("/api/admin", admin)
router.use("/api/message", message)
router.use("/api/chat", apiChat)
 
// 配置路由
app.use(router.routes()).use(router.allowedMethods());


// socket.io
const server = require('http').createServer(app.callback());
// // 监听端口
server.listen(33335, () => {
  console.log('listening on *:33335');
});
// 更改socket.io的默认通信方式transports，使用websocket模式transports: ['websocket']}

// const io = require('socket.io')(server, {transports: ['websocket']});
const io = require('socket.io')(server, {
  cors: true
});

const chatSocket = require("./controllers/chat");
io.on('connection', socket => {
  console.log('连接成功')
  // // 接收发送的数据
  socket.on('send_message',  data => {
  chatSocket.send_message(data,io)
})
io.emit('refresh', {
  'test': 1
})
  // socket.on('send_message', data => {
   
  //   console.log('收到123', data)
  //   io.emit('refresh', {
  //     'test': 1
  //   })
  // })
  // // 接收
  // socket.on('test2', data => {

  //   console.log('收到223', data)

  // })

 
})





// 设置端口号
const port = process.env.PORT || config.port;

app.listen(port, () => {
  console.log('serve start on ' + config.port)
});

