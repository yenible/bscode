/*
 * @Author: yenible
 * @Date: 2021-01-12 16:39:01
 * @LastEditors: yenible
 * @LastEditTime: 2021-04-18 20:18:52
 * @Description: xxx
 */
const Router = require('koa-router');
const mArticle = require('../../models/model_article')
const fs = require('fs');
const path = require('path');
const tool = require('../../lib/util/tool');
const stringVary = require('../../lib/util/stringVary')
const config = require('../../config/my_config')

// 实例化
const router = new Router();

router.post('/save_article', async (ctx) => {
  // ctx.set('Access-Control-Allow-Origin', '*');
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    let queryRes;
    let date = new Date()
    let INT_user_id = ctx.request.body.user_id;
    let article_title = ctx.request.body.article_title;
    let article_content = ctx.request.body.article_content;
    let publish_type = ctx.request.body.publish_type;
    let article_type = ctx.request.body.article_type;
    let article_state = ctx.request.body.article_state;
    let article_id = ctx.request.body.article_id;
    let label_list = ctx.request.body.label_list

    let labelArr = [];
    if (label_list) {
      // 将标签列表变为数组
      label_list.forEach(item => {
        labelArr.push(item.label_id * 1)
      })
    }

    // 转义md文章中的特殊字符
    article_content = stringVary.in(article_content);
    article_title = stringVary.in(article_title);




    // 文章发布再填该参数，且是第一次发布的时间
    let publish_time = ctx.request.body.publish_time || tool.format(date);
    let update_time = tool.format(date);




    if (!article_id) {
      ctx.body = {
        errCode: 1,
        msg: '找不到文章id'
      }
      return;
    }
    let isexist = await check_article(article_id);
    // console.log(isexist)
    if (isexist) {
      let updateData = {
        update_time,
        article_title,
        article_type,
        article_state,
        article_content,
        publish_type
      };
      console.log(isexist)
      if (article_state === '1') {
        updateData['publish_time'] = publish_time;
        // labelArr内容要存入文章标签表
        let labelRes = await save_label_info(labelArr, article_id, true);
        if (isexist.article_state === '0') {
          // 即代表文章从草稿箱发布
          //更新user表的article_amount数据
          await mArticle.update_article_amount(INT_user_id, 'add').then(res => {
            queryRes = res;
            // console.log(res)
          }).catch(err => {
            console.log(err);
            queryRes = {
              msg: err,
              errCode: 1
            };
          })
        }
        if (labelRes.errCode === 1) {
          ctx.body = {
            errCode: 1,
            msg: '文章发布失败，label标签保存错误',
            err: labelRes.err
          }
          return;
        }
      }
      await mArticle.update_article(updateData, article_id).then(result => {
        // console.log(result)
        if (result.affectedRows >= 0) {
          // 成功更新数据
          if (article_state === '1') {
            queryRes = {
              errCode: 0,
              msg: '文章发布成功',
              action: 'update'
            }
          } else {
            queryRes = {
              errCode: 0,
              msg: '文章更新成功',
              action: 'update'
            }
          }

        } else {
          queryRes = {
            errCode: 1,
            msg: '文章更新失败，数据库影响行数出错'
          }
        }
      }).catch(err => {
        // console.log(err)
        queryRes = {
          errCode: 1,
          msg: '文章更新失败，数据库影响行数出错',
          errmsg: err
        }
      })
      ctx.body = queryRes;
    } else {
      let articleObj = {
        INT_user_id,
        article_title,
        article_content,
        publish_type,
        article_type,
        article_state,
        update_time,
        article_id,
      }
      if (article_state === '1') {
        articleObj['publish_time'] = publish_time;
        // labelArr内容要存入文章标签表
        let labelRes = await save_label_info(labelArr, article_id, false);
        if (labelRes.errCode === 1) {
          ctx.body = {
            errCode: 1,
            msg: '文章发布失败，label标签保存错误',
            err: labelRes.err
          }
          return;
        }
      }
      await mArticle.add_article(articleObj).then(res => {
        queryRes = res;
        // console.log(res)
      }).catch(err => {
        console.log(err);
        queryRes = {
          msg: err,
          errCode: 1
        };
      })
      //更新user表的article_amount数据
      await mArticle.update_article_amount(INT_user_id, 'add').then(res => {
        queryRes = res;
        console.log(res)
      }).catch(err => {
        console.log(err);
        queryRes = {
          msg: err,
          errCode: 1
        };
      })
      if (queryRes.errCode === 1) {
        ctx.body = queryRes;
        return;
      }
      if (queryRes.affectedRows && queryRes.affectedRows == 1) {
        ctx.status = 200;
        if (article_state === '1') {
          ctx.body = {
            errCode: 0,
            msg: '文章发布成功',
            res: queryRes,
            action: 'save'
          }
        } else {
          ctx.body = {
            errCode: 0,
            msg: '文章保存成功',
            res: queryRes,
            action: 'save'
          }
        }

      } else {
        ctx.status = 200;
        ctx.body = {
          errCode: 1,
          msg: 'sql错误，affectedRows有问题'
        }
      }
    }
  }
});
router.post('/query_user_article', async (ctx) => {
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    let userid = ctx.request.body.user_id;
    let conditionObj = ctx.request.body.condition
    let pagesize = ctx.request.body.pagesize || 10
    let pageno = ctx.request.body.pageno || 0
    let params = ['article_id', 'article_content', 'update_time', 'article_title', 'publish_type', 'article_type', 'article_state', 'like_amount', 'bower_amount', 'article_collect_amount']
    let sort = ctx.request.body.sort
    let queryRes;
    let sortCol = ' update_time ';
    if (sort === 'hot') {
      sortCol = ' bower_amount '
    }
    // console.log(1232)
    // console.log(userid)

    // 查询当前用户id文章
    await mArticle.query_user_article(userid, conditionObj, params, pagesize, pageno, sortCol).then(async res => {
      let data = res;
      await mArticle.query_user_article_total(userid, conditionObj).then(countRes => {
        if (countRes[0] && countRes[0]['count(*)'] > -1) {
          queryRes = {
            errCode: 0,
            data: data,
            total: countRes[0]['count(*)'],
            msg: 'success'
          }
        } else {
          queryRes = {
            errCode: 1,
            err: countRes[0],
            msg: 'total查询失败'
          }
        }

      }).catch(err => {
        console.log(err)
        queryRes = {
          errCode: 1,
          err: err,
          msg: 'total查询失败'
        }
      })

    }).catch(err => {
      console.log(err)
      queryRes = {
        errCode: 1,
        err: err,
        msg: 'error'
      }
    })
    ctx.body = queryRes
  }
})
// 通过用户名获取文章数据
router.post('/query_username_article', async (ctx) => {

  let auth_name = ctx.request.body.auth_name;
  let conditionObj = ctx.request.body.condition
  let pagesize = ctx.request.body.pagesize || 10
  let pageno = ctx.request.body.pageno || 0
  let params = ['article_id', 'update_time', 'article_title', 'publish_type', 'article_type', 'article_state', 'like_amount', 'bower_amount', 'article_collect_amount']
  let sort = ctx.request.body.sort
  let queryRes;
  let sortCol = ' update_time ';
  if (sort === 'hot') {
    sortCol = ' bower_amount '
  }
  // console.log(1232)
  // console.log(userid)

  // 查询当前用户id文章
  await mArticle.query_username_article(auth_name, conditionObj, params, pagesize, pageno, sortCol).then(async res => {
    let data = res;
    await mArticle.query_username_article_total(auth_name, conditionObj).then(countRes => {
      // console.log(countRes)
      if (countRes[0] && countRes[0]['count(bs_article.article_id)'] > -1) {
        queryRes = {
          errCode: 0,
          data: data,
          total: countRes[0]['count(bs_article.article_id)'],
          msg: 'success'
        }
      } else {
        queryRes = {
          errCode: 1,
          err: countRes[0],
          msg: 'total查询失败'
        }
      }

    }).catch(err => {
      console.log(err)
      queryRes = {
        errCode: 1,
        err: err,
        msg: 'total查询失败'
      }
    })

  }).catch(err => {
    console.log(err)
    queryRes = {
      errCode: 1,
      err: err,
      msg: 'error'
    }
  })
  ctx.body = queryRes

})
router.post('/delete_article', async (ctx) => {
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    let article_id = ctx.request.body.article_id;
    let user_id = ctx.request.body.user_id;

    console.log(user_id)
    let delRes;
    await mArticle.delete_article(article_id).then(async res => {
      if (res.affectedRows < 1) {
        delRes = {
          errCode: 1,
          err: err,
          msg: '删除失败'
        }
        isError = true;
      } else {
        //更新user表的article_amount数据
        await mArticle.update_article_amount(user_id, 'remove').then(res => {
          queryRes = res;
          console.log(res)
        }).catch(err => {
          console.log(err);
          queryRes = {
            msg: err,
            errCode: 1
          };
        })
        delRes = {
          errCode: 0,
          msg: '文章删除成功'
        }
      }

    }).catch(err => {
      console.log(err);
      isError = true;
      delRes = {
        errCode: 1,
        err: err,
        msg: '删除失败'

      }
    })


  async function deleteall(path) {
	var files = [];
	if(fs.existsSync(path)) {
		files = fs.readdirSync(path);
		files.forEach(function(file, index) {
			var curPath = path + "/" + file;
			if(fs.statSync(curPath).isDirectory()) { // recurse
				deleteall(curPath);
			} else { // delete file
				fs.unlinkSync(curPath);
			}
		});
		fs.rmdirSync(path);
	}
}
    if(delRes['errCode']===0){
      // 删除文章的图片文件夹
      let path = config.articleImgSrc + "/" + article_id
      try{
       await deleteall(path)
      }catch(err){
        console.log(err)
      }
    }

    ctx.body = delRes;
  }
})
async function save_label_info(labelArr, article_id, isupdate) {
  let saveRes;
  let isErr = false;
  let arr = []
  // console.log(labelArr)
  await mArticle.query_article_label(article_id).then(res => {
    // arr = res
    res.forEach(item => {
      arr.push(item.label_id * 1)
    })
    // console.log(arr)

  }).catch(err => {
    isErr = true;
    saveRes = {
      errCode: 1,
      err: err
    }
  })
  for (let i = 0; i < labelArr.length; i++) {
    if (isErr) {
      break;
    }
    if (arr.indexOf(labelArr[i] * 1) !== -1) {
      continue;
    }
    await mArticle.save_label(labelArr[i] * 1, article_id).then(res => {
      // console.log(res)
      if (res.affectedRows < 1) {
        saveRes = {
          errCode: 1,
          err: err
        }
        isErr = true;
      }

    }).catch(err => {
      console.log(err);
      isErr = true;
      saveRes = {
        errCode: 1,
        err: err
      }
    })
  }
  // 若是处于更新发布文章状态，需要检测是否有的标签之前被选择而现在却没有被选择
  if (isupdate) {
    let nArr = arr.filter(function (item) {
      return labelArr.indexOf(item) == -1
    });
    console.log(nArr);
    let isError = false;
    // 需要删除nArr的标签内容
    for (let i = 0; i < nArr.length; i++) {
      console.log(nArr);

      if (isError) {
        console.log(nArr);

        break;
      }
      await mArticle.remove_label(nArr[i] * 1, article_id).then(res => {
        console.log(res)
        if (res.affectedRows < 1) {
          saveRes = {
            errCode: 1,
            err: err
          }
          isError = true;
        }

      }).catch(err => {
        console.log(err);
        isError = true;
        saveRes = {
          errCode: 1,
          err: err
        }
      })
    }
  }
  saveRes = {
    errCode: 0,
  }
  // console.log(saveRes)
  return saveRes;
}
async function check_article(article_id) {
  let checkRes = null
  //检测当前文章id是否已经保存
  await mArticle.query_article(article_id, ['article_id', 'article_state']).then(res => {
    if (res[0] && res[0].article_id && res[0].article_id === article_id) {
      checkRes = {
        article_state: res[0].article_state
      };
    }
  }).catch(err => {
    console.log(err);

  })
  return checkRes
}
//文章详情接口，获取当前文章所有有关信息
router.post('/get_article_detail', async (ctx) => {
  let article_id = ctx.request.body.article_id;
  let authname = ctx.request.body.authname;
  let type = ctx.request.body.type;
  let queryRes = {};
  let tokenRes = {};
  // console.log(type)
  // if (type === 'blog') {
  //   tokenRes['code'] = 0
  //   // console.log(333)
  // } else {
  //   tokenRes = tool.checkToken(ctx);
  // }
  // if ( tokenRes.code === 1) {
  //   ctx.body = tokenRes.body;
  // } else {
  await mArticle.add_article_bower(article_id).then(res => {}).catch(err => {
    isErr = true;
    queryRes = {
      errCode: 1,
      err: err,
      msg: '浏览数据增加失败'
    }
  })
  if (queryRes['err']) {
    ctx.body = queryRes;
    return;
  }
  await mArticle.get_article_detail(article_id).then(async result => {
    let data = result[0];
    let arr = [];
    let isErr = false;
    // console.log(labelArr)
    if (data.article_state === '0') {
      // 文章处于未发布状态
      queryRes = {
        errCode: 1,
        msg: '文章无法查看或不存在'
      }

    } else {
      await mArticle.query_article_label(article_id).then(res => {
        // arr = res
        res.forEach(item => {
          arr.push(item.label_id * 1)
        })
        // console.log(arr)
      }).catch(err => {
        isErr = true;
        queryRes = {
          errCode: 1,
          err: err
        }
      })
      if (!isErr) {
        data['label_list'] = arr;
        let isErrLabel = false;
        let dataObj = {}
        await mArticle.get_label().then(async labeRes => {
          // 返回拿到的树数据数组
          // 整理数据
          labeRes.forEach(element => {
            dataObj[element.label_id] = {
              label_name: element.label_name,
              label_type: element.label_type
            }

          });
        }).catch(err => {
          isErrLabel = true;
          queryRes = {
            errCode: 1,
            err: err
          }
        })

        if (!isErrLabel) {
          data['all_label'] = dataObj;
          queryRes = {
            errCode: 0,
            data: data
          }
        }
      }

    }
  }).catch(err => {
    queryRes = {
      errCode: 1,
      err: err,
      msg: '文章数据获取失败'
    }
  });

  ctx.body = queryRes;

})

// 点赞接口 start

// 点赞操作
router.post('/action_like', async (ctx) => {
  let article_id = ctx.request.body.article_id;
  let user_id = ctx.request.body.user_id;
  let auth_id = ctx.request.body.auth_id;
  let is_like = ctx.request.body.is_like;
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    let likeRes = {
      errCode: 1,
      msg: 'error'
    };
    // console.log(is_like)
    if (is_like === 'true') {
      await mArticle.create_like(user_id, article_id).then(async res => {
        if (res.affectedRows === 1) {
          await mArticle.update_like(article_id, is_like).then(result => {
            if (result.affectedRows === 1) {
              likeRes = {
                errCode: 0,
                msg: 'success',
                action: 'add'
              };
            }
          })
          let time = tool.format(new Date());
          // 检查bs_like_message是否已经存在该消息
          await mArticle.query_message_article(article_id, user_id).then(async qRes => {
            if (qRes[0] && qRes[0]['message_id']) {
              // 更新bs_like_message
              await mArticle.update_message_article(qRes[0]['message_id'], time).then(saveRes => {
                // console.log(saveRes)
                if (saveRes.affectedRows !== 1) {
                  likeRes = {
                    errCode: 1,
                    msg: '更新点赞文章消息出错',
                    err: saveRes,
                  };
                }
              }).catch(err => {
                console.log(err)
                likeRes = {
                  errCode: 1,
                  msg: '更新点赞文章消息出错',
                  err: err,
                };
              })
            } else {
              // 添加点赞消息
              await mArticle.save_message_article(article_id, auth_id, user_id, time).then(saveRes => {
                // console.log(saveRes)
                if (saveRes.affectedRows !== 1) {
                  likeRes = {
                    errCode: 1,
                    msg: '保存点赞文章消息出错',
                    err: saveRes,
                  };
                }
              }).catch(err => {
                console.log(err)
                likeRes = {
                  errCode: 1,
                  msg: '保存点赞文章消息出错',
                  err: err,
                };
              })
            }
          }).catch(err => {
            console.log(err)
            likeRes = {
              errCode: 1,
              msg: 'query_message_article消息出错',
              err: err,
            };
          })
        }
        // console.log(res)
      }).catch(err => {
        console.log(err)
      })
    } else {
      // console.log(33)
      await mArticle.delete_like(user_id, article_id).then(async res => {
        if (res.affectedRows === 1) {
          await mArticle.update_like(article_id, is_like).then(result => {
            if (result.affectedRows === 1) {
              likeRes = {
                errCode: 0,
                action: 'cancel',
                msg: 'success'
              };
            }
          })
        }
        // console.log(res)
      }).catch(err => {
        console.log(err)
      })
    }
    // 查询当前用户是否已经点赞
    await mArticle.query_like(user_id, article_id).then(async res => {
      if (res[0]) {
        likeRes['islike'] = true;
      } else {
        likeRes['islike'] = false;
      }
    })
    // 查询当前文章的点赞数量
    await mArticle.query_like_collect_amount(article_id).then(res => {

      if (res[0] && res[0].like_amount > -1) {
        likeRes['like_amount'] = res[0].like_amount;
      }
    }).catch(err => {
      console.log(err)
    })

    ctx.body = likeRes;
  }
})
// 获取文章是否已经点赞
router.post('/article_is_like', async (ctx) => {
  let article_id = ctx.request.body.article_id;
  let user_id = ctx.request.body.user_id;
  let queryRes = {
    errCode: 1,
    msg: 'error'
  };
  // 查询当前用户是否已经点赞
  await mArticle.query_like(user_id, article_id).then(async res => {
    if (res[0]) {
      queryRes = {
        islike: true,
        errCode: 0,
        msg: 'success'
      };
    } else {
      queryRes = {
        islike: false,
        errCode: 0,
        msg: 'success'
      };
    }
  }).catch(err => {
    console.log(err);
    queryRes['err'] = err
  })
  ctx.body = queryRes
})

// 点赞接口end

// 收藏接口start
router.post('/action_collect', async (ctx) => {
  let article_id = ctx.request.body.article_id;
  let user_id = ctx.request.body.user_id;
  let is_collect = ctx.request.body.is_collect;
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    let collectRes = {
      errCode: 1,
      msg: 'error'
    };
    // console.log(is_collect)
    if (is_collect === 'true') {
      let time = tool.format(new Date());
      await mArticle.create_collect(user_id, article_id, time).then(async res => {
        if (res.affectedRows === 1) {
          await mArticle.update_collect(article_id, is_collect).then(result => {
            if (result.affectedRows === 1) {
              collectRes = {
                errCode: 0,
                msg: 'success',
                action: 'add'
              };
            }
          })
        }
        // console.log(res)
      }).catch(err => {
        console.log(err)
      })
    } else {
      await mArticle.delete_collect(user_id, article_id).then(async res => {
        if (res.affectedRows === 1) {
          await mArticle.update_collect(article_id, is_collect).then(result => {
            if (result.affectedRows === 1) {
              collectRes = {
                errCode: 0,
                action: 'cancel',
                msg: 'success'
              };
            }
          })
        }
        // console.log(res)
      }).catch(err => {
        console.log(err)
      })
    }
    // 查询当前用户是否已经收藏
    await mArticle.query_collect(user_id, article_id).then(async res => {
      if (res[0]) {
        collectRes['iscollect'] = true;
      } else {
        collectRes['iscollect'] = false;
      }
    })
    // 查询当前文章的点赞数量
    await mArticle.query_like_collect_amount(article_id).then(res => {
      // console.log(res)
      if (res[0] && res[0].article_collect_amount > -1) {
        collectRes['collect_amount'] = res[0].article_collect_amount;
      }
    }).catch(err => {
      console.log(err)
    })

    ctx.body = collectRes;
  }
})

// 获取文章是否已经点赞
router.post('/article_is_collect', async (ctx) => {
  let article_id = ctx.request.body.article_id;
  let user_id = ctx.request.body.user_id;
  let queryRes = {
    errCode: 1,
    msg: 'error'
  };
  // 查询当前用户是否已经收藏
  await mArticle.query_collect(user_id, article_id).then(async res => {
    if (res[0]) {
      queryRes = {
        iscollect: true,
        errCode: 0,
        msg: 'success'
      };
    } else {
      queryRes = {
        iscollect: false,
        errCode: 0,
        msg: 'success'
      };
    }
  }).catch(err => {
    console.log(err);
    queryRes['err'] = err
  })
  ctx.body = queryRes
})
// 收藏接口 end

// 关注接口start
router.post('/action_attract', async (ctx) => {
  let is_attract = ctx.request.body.is_attract
  let user_id = ctx.request.body.user_id
  let auth_id = ctx.request.body.auth_id
  let tokenRes = tool.checkToken(ctx);

  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    let attractRes = {
      errCode: 1,
      msg: 'error',
    };
    // 关注分三步
    // 1、创建关注表的记录
    // 2、更改user的关注数量
    // 3、修改auth的粉丝数量
    await mArticle.change_attract(user_id, auth_id, is_attract).then(async cres => {
      if (cres.affectedRows === 1) {
        // 2、更改user的关注数量
        await mArticle.update_focus_amount(user_id, is_attract).then(async focres => {
          if (focres.affectedRows === 1) {
            // 3、修改auth的粉丝数量
            await mArticle.update_fans_amount(auth_id, is_attract).then(async fanres => {
              if (fanres.affectedRows === 1) {
                attractRes = {
                  errCode: 0,
                  msg: 'success',
                }
                if (is_attract === 'true') {
                  attractRes['action'] = 'add'



                  // 添加关注信息
                  let time = tool.format(new Date());
                  // 检查bs_like_message是否已经存在该消息
                  await mArticle.query_message_attract(user_id, auth_id).then(async qRes => {
                    if (qRes[0] && qRes[0]['message_id']) {
                      // 更新bs_like_message
                      await mArticle.update_message_attract(qRes[0]['message_id'], time).then(saveRes => {
                        // console.log(saveRes)
                        if (saveRes.affectedRows !== 1) {
                          likeRes = {
                            errCode: 1,
                            msg: '更新关注消息出错',
                            err: saveRes,
                          };
                        }
                      }).catch(err => {
                        console.log(err)
                        likeRes = {
                          errCode: 1,
                          msg: '更新关注消息出错',
                          err: err,
                        };
                      })
                    } else {
                      // 添加点赞消息
                      await mArticle.save_message_attract(user_id, auth_id, time).then(saveRes => {
                        // console.log(saveRes)
                        if (saveRes.affectedRows !== 1) {
                          likeRes = {
                            errCode: 1,
                            msg: '保存关注消息出错',
                            err: saveRes,
                          };
                        }
                      }).catch(err => {
                        console.log(err)
                        likeRes = {
                          errCode: 1,
                          msg: '保存关注消息出错',
                          err: err,
                        };
                      })
                    }
                  }).catch(err => {
                    console.log(err)
                    likeRes = {
                      errCode: 1,
                      msg: 'query_message_attract消息出错',
                      err: err,
                    };
                  })




                } else {
                  attractRes['action'] = 'cancel'
                }
              }
            })
          }
        })
      }
    }).catch(err => {
      attractRes['err'] = err;
      console.log(err)
    })
    ctx.body = attractRes
  }
})

// 获取文章是否已经点赞
router.post('/article_is_attract', async (ctx) => {
  let user_id = ctx.request.body.user_id
  let auth_id = ctx.request.body.auth_id
  let queryRes = {
    errCode: 1,
    msg: 'error'
  };
  // 查询当前用户是否已经关注
  await mArticle.query_attract(user_id, auth_id).then(async res => {
    if (res[0]) {
      queryRes = {
        isAttract: true,
        errCode: 0,
        msg: 'success'
      };
    } else {
      queryRes = {
        isAttract: false,
        errCode: 0,
        msg: 'success'
      };
    }
  }).catch(err => {
    console.log(err);
    queryRes['err'] = err
  })
  ctx.body = queryRes
})
// 关注接口 end

// 获取文章信息,用于修改文章时获取内容
router.post('/get_article_info', async (ctx) => {
  let queryRes = {}
  let url_data = ctx.request.body.url_data.substring(1).split("/md/");
  // console.log(url_data)
  if (!url_data[1] || !url_data[0]) {
    ctx.body = {
      errCode: 1,
      msg: '参数传入错误'
    }
  }
  let article_id = url_data[1];
  let user_id = url_data[0];
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    // console.log(ctx.request)
    if (tokenRes.id * 1 !== user_id * 1) {
      queryRes = {
        errCode: 3,
        msg: '该文章不属于当前用户或url错误'
      }
      ctx.body = queryRes;
      return;
    }
    // console.log(url_data)
    await mArticle.query_article(article_id, ['article_id', 'article_content', 'article_title', 'user_id', 'publish_type']).then(async res => {
      console.log(res);

      if (res[0] && res[0].article_id && res[0].article_id === article_id) {
        // 判断文章的用作者是否一致
        console.log(user_id)
        console.log(res[0].user_id)
        if (user_id + "" != res[0].user_id + "") {
          queryRes = {
            errCode: 3,
            msg: '该文章不属于当前用户或url错误'
          }

        } else {
          let arr = [];
          let isErr = false;
          // console.log(labelArr)
          await mArticle.query_article_label(article_id).then(res => {
            // arr = res
            res.forEach(item => {
              arr.push(item.label_id * 1)
            })
            // console.log(arr)
          }).catch(err => {
            isErr = true;
            saveRes = {
              errCode: 1,
              err: err
            }
          })
          if (!isErr) {
            queryRes = {
              articleinfo: {
                title: stringVary.out(res[0].article_title),
                content:res[0].article_content? stringVary.out(res[0].article_content):'',
                label_list: arr
              },
              errCode: 0,
            }
          }

        }
      } else {
        queryRes = {
          errCode: 1,
          msg: '当前文章id未保存',
          exist:1
        }
      }
    }).catch(err => {
      console.log(err)
      queryRes = {
        errCode: 2,
        msg: '查询出错',
        err: err
      }
    });
    // 查询文章的标签


    ctx.body = queryRes
  }
});
// 获取关注列表
router.post('/get_attract_list', async ctx => {
  let user_id = ctx.request.body.user_id
  let queryRes = {
    errCode: 1,
    msg: 'error'
  }
  let tokenRes = tool.checkToken(ctx);

  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    await mArticle.query_attract_list(user_id).then(res => {
      console.log(res)
      let attractlists = []
      res.forEach(item => {
        attractlists.push({
          authId: item.auth_id,
          authName: item.name,
          avatarUrl: item.avatar
        })
      })
      queryRes = {
        errCode: 0,
        msg: 'success',
        data: attractlists
      }
    }).catch(err => {
      console.log(err)
      queryRes['err'] = err
    })
    ctx.body = queryRes

  }
})

// 获取粉丝列表
router.post('/get_fan_list', async ctx => {
  let user_id = ctx.request.body.user_id
  let queryRes = {
    errCode: 1,
    msg: 'error'
  }
  let tokenRes = tool.checkToken(ctx);

  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    await mArticle.query_fans_list(user_id).then(res => {
      console.log(res)
      let lists = []
      res.forEach(item => {
        lists.push({
          userId: item.user_id,
          userName: item.name,
          avatarUrl: item.avatar

        })
      })
      queryRes = {
        errCode: 0,
        msg: 'success',
        data: lists
      }
    }).catch(err => {
      console.log(err)
      queryRes['err'] = err
    })
    ctx.body = queryRes

  }
})

// 取消关注接口
router.post('/cancel_attract', async ctx => {
  let auth_id = ctx.request.body.auth_id;
  let user_id = ctx.request.body.user_id;
  let delRes = {
    errCode: 1,
    msg: 'error'
  }
  await mArticle.delete_attract(auth_id, user_id).then(res => {
    if (res.affectedRows === 1) {
      delRes = {
        errCode: 0,
        msg: 'cancel success'
      }
    }
  }).catch(err => {
    delRes['err'] = err
  });
  ctx.body = delRes
})

// 获取标签信息
router.get('/get_label_info', async (ctx) => {
  let article_id = ctx.query.article_id
  let labelInfo;
  console.log(article_id)

  await mArticle.get_label().then(async res => {
    // 返回拿到的树数据数组
    // 整理数据
    let dataObj = {}
    // console.log(res)
    let labelType = {}
    res.forEach(element => {
      // console.log(element)
      if (dataObj[element.type_name]) {
        dataObj[element.type_name].push({
          label_id: element.label_id,
          label_name: element.label_name,
          label_type: element.label_type
        })
      } else {
        dataObj[element.type_name] = [{
          label_id: element.label_id,
          label_name: element.label_name,
          label_type: element.label_type
        }]
        labelType[element.type_name] = element.label_type
      }
    });
    // allLabelInfo = dataObj
    let arr = []
    // console.log(labelArr)
    if (article_id) {


      await mArticle.query_article_label(article_id).then(res => {
        // arr = res
        console.log(res)
        res.forEach(item => {
          arr.push(item.label_id * 1)
        })
        // console.log(arr)
      }).catch(err => {
        console.log(err)
        labelInfo = {
          errCode: 1,
          err: err,
          msg: 'label查询出错'
        }
      })
      labelInfo = {
        errCode: 0,
        msg: 'success',
        data: {
          allLabel: dataObj,
          selectedLabel: arr,
          labelType:labelType
        }
      };
    } else {
      labelInfo = {
        errCode: 0,
        msg: 'success',
        data: {
          allLabel: dataObj,
        }
      };
    }

    // console.log(dataObj)
  }).catch(err => {
    labelInfo = {
      errCode: 1,
      msg: "查询数据库失误"
    }
    console.log(err)
  })
  ctx.body = labelInfo;
})
router.get('/get_article_type', async (ctx) => {
  let labelInfo;
  await mArticle.get_article_type_list().then(async res => {
    // console.log(res)
    labelInfo = {
      errCode: 0,
      msg: 'success',
      type_list: res,
    };
  }).catch(err => {
    labelInfo = {
      errCode: 1,
      msg: "查询数据库失误"
    }
    console.log(err)
  })
  ctx.body = labelInfo;
})
// 按照指定顺序获取特定类型文章信息
router.post('/get_article_sort', async (ctx) => {
  let article_type = ctx.request.body.article_type;
  let sortStr = ctx.request.body.sort;
  let pagesize = ctx.request.body.pagesize || 10
  let pageno = ctx.request.body.pageno || 0

  let queryRes = {
    errCode: 1,
    msg: 'error'
  }
  await mArticle.get_article_sort(article_type, sortStr, pageno, pagesize).then(async res => {
    // console.log(res)
    let data = res
    //查询文章总数
    await mArticle.get_article_sort_total(article_type).then(countRes => {
      // console.log(countRes)
      if (countRes[0] && countRes[0]['count(bs_article.article_id)'] > -1) {
        queryRes = {
          errCode: 0,
          data: data,
          total: countRes[0]['count(bs_article.article_id)'],
          pagesize: pagesize,
          msg: 'success'
        }
      } else {
        queryRes = {
          errCode: 1,
          err: countRes,
          msg: 'total查询失败'
        }
      }

    }).catch(err => {
      console.log(err)
      queryRes = {
        errCode: 1,
        err: err,
        msg: 'total查询失败'
      }
    })

  }).catch(err => {
    queryRes['err'] = err
    queryRes['msg'] = '获取博客信息失败'
  })
  ctx.body = queryRes
})
router.post('/query_collect_article', async (ctx) => {
  let user_id = ctx.request.body.user_id;
  let pagesize = ctx.request.body.pagesize || 10
  let pageno = ctx.request.body.pageno || 0
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    let queryRes = {
      errCode: 1,
      msg: 'error'
    }
    await mArticle.query_collect_article(user_id, pageno, pagesize).then(async res => {
      // console.log(res)
      let data = res;
      await mArticle.query_collect_article_total(user_id).then(countRes => {
        console.log(countRes)
        if (countRes[0] && countRes[0]['count(*)'] > -1) {
          queryRes = {
            errCode: 0,
            data: data,
            total: countRes[0]['count(*)'],
            msg: 'success'
          }
        } else {
          queryRes = {
            errCode: 1,
            err: countRes[0],
            msg: 'total查询失败'
          }
        }

      }).catch(err => {
        console.log(err)
        queryRes = {
          errCode: 1,
          err: err,
          msg: 'total查询失败'
        }
      })
    }).catch(err => {
      console.log(err)
      queryRes['msg'] = '文章获取失败';
      queryRes['err'] = err;

    })
    ctx.body = queryRes
  }

})

router.post('/create_label', async (ctx) => {
  let type_id = ctx.request.body.type_id;
  let label_name = ctx.request.body.label_name
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    let createRes = {
      errCode:1,
      msg:'error'
    }
    await mArticle.create_label(label_name,type_id).then(res=>{
      console.log(res)
      if(res.affectedRows*1>-1){
        createRes = {
          errCode:0,
          msg:'添加成功'
        }
      }else{
        createRes['msg']='添加失败'
        createRes['err']=res
      }
    }).catch(err=>{
      createRes['msg']='添加失败'
        createRes['err']=err
    })
    // console.log(type_id)
    // console.log(label_name)
    ctx.body = createRes
  }
})
module.exports = router.routes();