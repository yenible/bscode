/*
 * @Author: yenible
 * @Date: 2020-12-13 22:19:04
 * @LastEditors: yenible
 * @LastEditTime: 2021-04-05 11:06:10
 * @Description: 用户路由文件
 */
// 引用koa-router
const Router = require('koa-router');
const mUser = require('../../models/model_user')
const crypto = require('crypto'); //导入加密模块
const jwt = require('jsonwebtoken'); //token生成插件
const fs = require('fs');
const path = require('path');
const tool = require('../../lib/util/tool')

// 实例化
const router = new Router();

// users的路由

// 注册接口
router.post('/register', async (ctx) => {
    // 获取到前端的数据后，到数据库中进行查询
    // 查询当前的email和name是否已经注册过
    let queryEmail;
    let queryName;
    // 查询名称是否重复
    await mUser.find_user({
        name: ctx.request.body.name
    }, ['email', 'name', 'id']).then(res => {
        // console.log(res);
        queryName = res;
    }).catch(err => {
        console.log(err);
        queryName = {
            msg: err,
            errCode: 1
        };
    })
    // 查询邮箱是否重复
    await mUser.find_user({
        email: ctx.request.body.email
    }, ['email', 'name', 'id']).then(res => {
        // console.log(res);
        queryEmail = res;
    }).catch(err => {
        console.log(err);
        queryEmail = {
            msg: err,
            errCode: 1
        };
    })
    // 查询出现问题则直接返回错误内容
    if (queryEmail.errCode === 1) {
        ctx.body = queryEmail;
        return;
    }
    if (queryName.errCode === 1) {
        ctx.body = queryName;
        return;
    }
    if (queryName.length > 0) {
        ctx.status = 200;
        ctx.body = {
            errCode: 1,
            msg: '用户名已存在',
            errObj: 'name'
        }
        return;
    } else if (queryEmail.length > 0) {
        ctx.status = 200;
        ctx.body = {
            errCode: 1,
            msg: '邮箱已被注册',
            errObj: 'email'
        }
        return;
    } else {
        let insertRes;
        // 获取加密后的密码
        let cryptObj = cryptPwd(ctx.request.body.password);
        // console.log(ctx.request.body)
        await mUser.register({
            name: ctx.request.body.name,
            email: ctx.request.body.email,
            password: cryptObj.password,
            salt: cryptObj.salt,
            sex: ctx.request.body.sex
        }).then(res => {
            insertRes = res;
        }).catch(err => {
            insertRes = {
                msg: err,
                errCode: 1,
                tip: '插入新数据时出错'
            };
            console.log(err)
        });
        if (insertRes.errCode === 1) {
            ctx.body = insertRes;
            return;
        }
        if (insertRes.affectedRows && insertRes.affectedRows == 1) {
            ctx.status = 200;
            ctx.body = {
                errCode: 0,
                msg: '注册成功',
                res: insertRes
            }
        } else {
            ctx.status = 200;
            ctx.body = {
                errCode: 1,
                msg: 'sql错误，affectedRows有问题'
            }
        }
    }
})
/**
 * @description: 登录的接口
 * @detail: 返回token
 */
// 登录接口
router.post('/login', async (ctx) => {
    //通过用户名查询用户
    let queryRes;
    await mUser.find_user({
        name: ctx.request.body.name
    }, ['email', 'salt', 'id', 'name']).then(res => {
        // console.log(res);
        queryRes = res;
    }).catch(err => {
        console.log(err);
        queryRes = {
            msg: err,
            errCode: 1
        };
    })
    // 查询出现问题则直接返回错误内容
    if (queryRes.errCode === 1) {
        ctx.body = queryRes;
        return;
    }
    if (queryRes.length == 0) {
        ctx.status = 200;
        ctx.body = {
            errCode: 1,
            msg: '用户未注册，找不到该账户',
            errObj: 'name'
        }
        return;
    } else {
        // 查到后验证密码
        let nowPassword = cryptPwd(ctx.request.body.password, queryRes[0].salt);
        // 验证密码
        let loginRes
        await mUser.find_user({
            name: ctx.request.body.name,
            password: nowPassword.password
        }, ['email', 'salt', 'id', 'name']).then(res => {
            // console.log(res);
            loginRes = res;
        }).catch(err => {
            console.log(err);
            loginRes = {
                msg: err,
                errCode: 1
            };
        })
        // 查询出现问题则直接返回错误内容
        if (loginRes.errCode === 1) {
            ctx.body = loginRes;
            return;
        }
        if (loginRes.length == 0) {
            ctx.status = 200;
            ctx.body = {
                errCode: 1,
                msg: '密码错误',
                errObj: 'password'
            }
            return;
        } else {
            let val = loginRes[0];
            // console.log(loginRes)
            let uid = val['id'];
            let token = generateToken({
                id: uid,
                name: val['name'],
        isAdmin:false

            });
            ctx.status = 200;
            ctx.body = {
                errCode: 0,
                msg: '登陆成功..',
                data: {
                    token,
                    id: val.id,
                    name: val['name']
                }
            }
            return;
        }
    }
})
// 修改用户密码
router.post('/update_password', async (ctx) => {
    let tokenRes = tool.checkToken(ctx);
    if (tokenRes.code === 1) {
        ctx.body = tokenRes.body;
    } else {
    // console.log(ctx)
    let queryRes;
    // 获取加密后的密码
    let cryptObj = cryptPwd(ctx.request.body.password);
    let id = ctx.request.body.id
    let updateObj = {
        password: cryptObj.password,
        salt: cryptObj.salt,
        id: id
    }
    // await mUser.find_user({
    await mUser.update_user(updateObj).then(res => {
        queryRes = res;
    }).catch(err => {
        console.log(err);
        queryRes = {
            msg: err,
            errCode: 1
        };
    })
    ctx.body = {
        errCode: 0,
        data: {
            info: queryRes,
            count: queryRes.length
        }
    }}
})
// 修改用户数据
router.post('/update_info', async (ctx) => {
    let tokenRes = tool.checkToken(ctx);
    if (tokenRes.code === 1) {
        ctx.body = tokenRes.body;
    } else {
        // console.log(ctx)
        let queryRes;
        //获取用户名称
        let updateObj = ctx.request.body

        ctx.body = Object.keys(updateObj)

        // await mUser.find_user({
        await mUser.update_user(updateObj).then(res => {
            queryRes = res;
        }).catch(err => {
            console.log(err);
            queryRes = {
                msg: err,
                errCode: 1
            };
        })
        ctx.body = {
            errCode: 0,
            data: {
                info: queryRes,
                count: queryRes.length
            }
        }
    }
})
// 验证token是否有效
router.post('/check_token', async (ctx) => {
    let tokenRes = tool.checkToken(ctx);
    console.log(tokenRes)
    if (tokenRes.code === 1) {
        // token无效
        ctx.body = tokenRes.body;
    }else{
        ctx.body = {
            errCode: 0,
            msg: "token有效",
            token: true
        }
    }
})
router.get('/get_user_info', async (ctx) => {
    let tokenRes = tool.checkToken(ctx)
    let id = ctx.query.id;

    if (tokenRes.code === 1) {
        ctx.body = tokenRes.body;
    } else if(tokenRes.id != id){
        ctx.body = {
            errCode: 1,
            msg: "请重新登录",
            redirect: '/login',
            errObj: 'login'
        }
    } else {
    // console.log(ctx)
    let queryRes;
    //获取用户id
     await mUser.find_user({
        INT_id: id
    }, ['id', 'email', 'name', 'strdescribe','avatar', 'sex', 'avatar', 'create_time', 'login_time', 'score', 'article_amount', 'focus_amount', 'user_collect_amount', 'fans_amount']).then(res => {
        queryRes = res;
    }).catch(err => {
        console.log(err);
        queryRes = {
            msg: err,
            errCode: 1
        };
    })
    ctx.body = {
        errCode: 0,
        data: queryRes
    }}
})

// 通过用户名获取用户数据
router.get('/get_username_info', async (ctx) => {
    let authname = ctx.query.authname;
    // console.log(ctx)
    let queryRes;
    //获取用户id
     await mUser.find_user({
        name: authname
    }, ['id', 'email', 'name', 'strdescribe','avatar', 'sex', 'avatar', 'create_time', 'login_time', 'score', 'article_amount', 'focus_amount', 'user_collect_amount', 'fans_amount']).then(res => {
        queryRes = res;
    }).catch(err => {
        console.log(err);
        queryRes = {
            msg: err,
            errCode: 1
        };
    })
    ctx.body = {
        errCode: 0,
        data: queryRes
    }
})

//生成token的方法
function generateToken(data) {
    let created = Math.floor(Date.now() / 1000);
    let cert = fs.readFileSync(path.join(__dirname, '../../config/key/rsa_private_key.pem')); //私钥
    let token = jwt.sign({
        data,
        exp: created + 3600 * 24
    }, cert, {
        algorithm: 'RS256'
    });
    return token;
}
// 检测用户名是否存在
router.get('/check_username', async (ctx) => {
    // console.log(ctx)
    let queryRes;
    //获取用户名称
    let name = ctx.query.name;
    await mUser.find_user({
        name: name
    }, ['email', 'name', 'id']).then(res => {
        queryRes = res;
    }).catch(err => {
        console.log(err);
        queryRes = {
            msg: err,
            errCode: 1
        };
    })
    ctx.body = {
        errCode: 0,
        data: {
            info: queryRes,
            count: queryRes.length
        }
    }
})

// 检测邮箱是否存在
router.get('/check_email', async (ctx) => {
    // console.log(ctx)
    let queryRes;
    //获取用户名称
    let email = ctx.query.email;
    await mUser.find_user({
        email: email
    }, ['email', 'name', 'id']).then(res => {
        queryRes = res;
    }).catch(err => {
        console.log(err);
        queryRes = {
            msg: err,
            errCode: 1
        };
    })
    ctx.body = {
        errCode: 0,
        data: {
            info: queryRes,
            count: queryRes.length
        }
    }
})

// 获取随机的盐值
function getRandomSalt() {
    return Math.random().toString().slice(2, 10);
}
// 密码加密，加盐加密
function cryptPwd(password, salt) {
    if (!salt) {
        salt = getRandomSalt();
    }
    let saltPassword = password + ':' + salt;
    // dm5加密
    let md5 = crypto.createHash('md5');
    let result = md5.update(saltPassword).digest('hex');
    return {
        password: result,
        salt: salt
    };

}

// 要调用routes方法才行
module.exports = router.routes();