/*
 * @Author: yenible
 * @Date: 2021-03-12 20:53:25
 * @LastEditors: yenible
 * @LastEditTime: 2021-04-20 20:41:51
 * @Description: xxx
 */

// 引用koa-router
const Router = require('koa-router');
const mMessage = require('../../models/model_message')
const fs = require('fs');
const path = require('path');
const tool = require('../../lib/util/tool');

// 实例化
const router = new Router();

//文章的评论消息
router.post('/get_message_comment', async (ctx) => {
  let user_id = ctx.request.body.userid;
  let queryRes = {
    errCode: 1,
    msg: 'error'
  }
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    let total = -1
    // 获取未查看的博客消息
    await mMessage.get_article_file_message_total(user_id).then(res => {
      if (res[0] && res[0]['count(*)'] * 1 > -1) {
        total = res[0]['count(*)']
      } else {
        queryRes['err'] = res;
        queryRes['msg'] = 'get_article_file_message_total出错';
      }
    }).catch(err => {
      console.log(err)
      queryRes['err'] = err;
      queryRes['msg'] = 'get_article_file_message_total出错';

    })
    if (total === -1) {
      ctx.body = queryRes;
      return;
    }
     
  
  // 主要信息来源有两种
  // 1、评论文章的信息
  // 2、评论用户评论的信息
  //同时需要区分file和article
  //返回的数据包括，文章id，被评论的评论id（若无则为-1），原评论内容，被评论内容及评论id以及评论者的名字与userid

  // 获取bs_article_file_message
  await mMessage.get_article_file_message(user_id).then(res => {
    // console.log(res)
    queryRes = {
      errCode: 0,
      data: res,
      msg: 'success',
      unread_total:total
    }
  }).catch(err => {
    console.log(err)
    queryRes['err'] = err;
    queryRes['msg'] = 'get_article_file_message出错';

  })
  ctx.body = queryRes
}
})

router.post('/delete_comment_message', async (ctx) => {
  let message_id = ctx.request.body.message_id;
  let queryRes = {
    errCode: 1,
    msg: 'error'
  }
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {

    await mMessage.delete_comment_message(message_id).then(res => {
      // console.log(res)
      if (res.affectedRows === 1) {
        queryRes = {
          errCode: 0,
          msg: '删除评论消息成功'
        }
      } else {
        queryRes['err'] = res;
        queryRes['msg'] = '删除评论消息出错'
      }

    }).catch(err => {
      console.log(err)
      queryRes['err'] = err;
      queryRes['msg'] = '删除评论消息出错';

    })
    ctx.body = queryRes
  }
})

// 点赞消息
router.post('/get_message_like', async (ctx) => {
  let user_id = ctx.request.body.userid;
  let queryRes = {
    errCode: 1,
    msg: 'error'
  }
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {


    let total = -1
    // 获取未查看的博客消息
    await mMessage.get_like_message_total(user_id).then(res => {
      if (res[0] && res[0]['count(*)'] * 1 > -1) {
        total = res[0]['count(*)']
      } else {
        queryRes['err'] = res;
        queryRes['msg'] = 'get_like_message_total出错';
      }
    }).catch(err => {
      console.log(err)
      queryRes['err'] = err;
      queryRes['msg'] = 'get_like_message_total出错';

    })
    if (total === -1) {
      ctx.body = queryRes;
      return;
    }


    let data = []
    await mMessage.get_like_message_article(user_id).then(res => {
      data = res
    }).catch(err => {
      console.log(err)
      queryRes['err'] = err;
      queryRes['msg'] = 'get_like_message_article出错';
    })
    if (queryRes['err']) {
      ctx.body = queryRes
      return;
    }

    await mMessage.get_like_message_file(user_id).then(res => {
      data = data.concat(res)
    }).catch(err => {
      console.log(err)
      queryRes['err'] = err;
      queryRes['msg'] = 'get_like_message_file出错';
    })
    if (queryRes['err']) {
      ctx.body = queryRes
      return;
    }

    data = data.sort(function (a, b) {
      return b.reply_time - a.reply_time;
    });

    queryRes = {
      errCode: 0,
      data: data,
      msg: 'success',
      unread_total:total
    }
    ctx.body = queryRes
  }
})

// 关注消息
router.post('/get_message_attract', async (ctx) => {
  let user_id = ctx.request.body.userid;
  let queryRes = {
    errCode: 1,
    msg: 'error'
  }
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {


    let total = -1
    // 获取未查看的博客消息
    await mMessage.get_attract_message_total(user_id).then(res => {
      if (res[0] && res[0]['count(*)'] * 1 > -1) {
        total = res[0]['count(*)']
      } else {
        queryRes['err'] = res;
        queryRes['msg'] = 'get_attract_message_total出错';
      }
    }).catch(err => {
      console.log(err)
      queryRes['err'] = err;
      queryRes['msg'] = 'get_attract_message_total出错';

    })
    if (total === -1) {
      ctx.body = queryRes;
      return;
    }


    await mMessage.get_message_attract(user_id).then(res => {
      // console.log(res)
      queryRes = {
        errCode: 0,
        data: res,
        msg: 'success',
      unread_total:total
      }
    }).catch(err => {
      console.log(err)
      queryRes['err'] = err;
      queryRes['msg'] = 'get_message_attract出错';

    })
    ctx.body = queryRes
  }
})

router.post('/delete_attract_message', async (ctx) => {
  let message_id = ctx.request.body.message_id;
  let queryRes = {
    errCode: 1,
    msg: 'error'
  }
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {

    await mMessage.delete_attract_message(message_id).then(res => {
      // console.log(res)
      if (res.affectedRows === 1) {
        queryRes = {
          errCode: 0,
          msg: '删除关注消息成功'
        }
      } else {
        queryRes['err'] = res;
        queryRes['msg'] = '删除关注消息出错'
      }

    }).catch(err => {
      console.log(err)
      queryRes['err'] = err;
      queryRes['msg'] = '删除关注消息出错';

    })
    ctx.body = queryRes
  }
})
router.post('/delete_like_message', async (ctx) => {
  let message_id = ctx.request.body.message_id;
  let queryRes = {
    errCode: 1,
    msg: 'error'
  }
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {

    await mMessage.delete_like_message(message_id).then(res => {
      // console.log(res)
      if (res.affectedRows === 1) {
        queryRes = {
          errCode: 0,
          msg: '删除点赞消息成功'
        }
      } else {
        queryRes['err'] = res;
        queryRes['msg'] = '删除点赞消息出错'
      }

    }).catch(err => {
      console.log(err)
      queryRes['err'] = err;
      queryRes['msg'] = '删除点赞消息出错';

    })
    ctx.body = queryRes
  }
})
 
router.post('/read_all_comment', async (ctx) => {
  let user_id = ctx.request.body.userid;
  let queryRes = {
    errCode: 1,
    msg: 'error'
  }
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {

    await mMessage.article_file_message_saw(user_id).then(res => {
      // console.log(res)
      if (res.affectedRows >= 0) {
        queryRes = {
          errCode: 0,
          msg: '评论消息已读成功'
        }
      } else {
        queryRes['err'] = res;
        queryRes['msg'] = '评论消息已读出错'
      }

    }).catch(err => {
      console.log(err)
      queryRes['err'] = err;
      queryRes['msg'] = '评论消息已读出错';

    })
    ctx.body = queryRes
  }
})
router.post('/read_all_like', async (ctx) => {
  let user_id = ctx.request.body.userid;
  let queryRes = {
    errCode: 1,
    msg: 'error'
  }
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {

    await mMessage.like_message_saw(user_id).then(res => {
      // console.log(res)
      if (res.affectedRows >= 0) {
        queryRes = {
          errCode: 0,
          msg: '点赞消息已读成功'
        }
      } else {
        queryRes['err'] = res;
        queryRes['msg'] = '点赞消息已读出错'
      }

    }).catch(err => {
      console.log(err)
      queryRes['err'] = err;
      queryRes['msg'] = '点赞消息已读出错';

    })
    ctx.body = queryRes
  }
})


router.post('/read_all_attract', async (ctx) => {
  let user_id = ctx.request.body.userid;
  let queryRes = {
    errCode: 1,
    msg: 'error'
  }
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {

    await mMessage.attract_message_saw(user_id).then(res => {
      console.log(res)
      if (res.affectedRows >= 0) {
        queryRes = {
          errCode: 0,
          msg: '关注消息已读成功'
        }
      } else {
        queryRes['err'] = res;
        queryRes['msg'] = '关注消息已读出错'
      }

    }).catch(err => {
      console.log(err)
      queryRes['err'] = err;
      queryRes['msg'] = '关注消息已读出错';

    })
    ctx.body = queryRes
  }
})

router.post('/submit_suggest', async (ctx) => {
  let user_id = ctx.request.body.user_id;
  let content = ctx.request.body.content;
  let title = ctx.request.body.title;
  let queryRes = {
    errCode: 1,
    msg: 'error'
  }
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
  let create_time = tool.format(new Date());

    await mMessage.submit_suggest(user_id,content,title,create_time).then(res => {
      console.log(res)
      if (res.affectedRows >= 0) {
        queryRes = {
          errCode: 0,
          msg: '建议/反馈已递交成功'
        }
      } else {
        queryRes['err'] = res;
        queryRes['msg'] = '建议/反馈递交出错'
      }

    }).catch(err => {
      console.log(err)
      queryRes['err'] = err;
      queryRes['msg'] = '建议/反馈递交出错';

    })
    ctx.body = queryRes
  }
 
   
})
router.post('/get_user_suggest', async (ctx) => {
  let user_id = ctx.request.body.user_id;
  let pageno = ctx.request.body.pageno || 0
  let pagesize = ctx.request.body.pagesize || 10
  let suggest_state = ctx.request.body.suggest_state || 0

  let queryRes = {
    errCode: 1,
    msg: 'error'
  }
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {

    await mMessage.get_user_suggest(user_id,suggest_state,pageno,pagesize).then(async res => {
      // console.log(res)
      await mMessage.get_user_suggest_total(user_id,suggest_state).then(async countRes => {
      console.log(countRes)
        if(countRes &&countRes[0]&&countRes[0]['count(*)']*1>-1){
          queryRes = {
            errCode: 0,
            msg: 'success',
            data:res,
            total:countRes[0]['count(*)']
          }
        }
      }).catch(err => {
        console.log(err)
        queryRes['err'] = err;
        queryRes['msg'] = 'get_user_suggest_total出错';
  
      })

    }).catch(err => {
      console.log(err)
      queryRes['err'] = err;
      queryRes['msg'] = 'get_user_suggest出错';

    })
    ctx.body = queryRes
  }
})
module.exports = router.routes();