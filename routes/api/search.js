/*
 * @Author: yenible
 * @Date: 2021-03-28 19:35:15
 * @LastEditors: yenible
 * @LastEditTime: 2021-04-14 13:58:16
 * @Description: xxx
 */

// 引用koa-router
const Router = require('koa-router');
const mSearch = require('../../models/model_search')
const fs = require('fs');
const path = require('path');
const tool = require('../../lib/util/tool');


// 实例化
const router = new Router();

//查询文章
router.post('/search_my_myblogs', async (ctx) => {
  let myblogsType = ctx.request.body.myblogsType
  let searchDate = ctx.request.body.searchDate
  let user_id = ctx.request.body.user_id
  let searchText = ctx.request.body.searchText
  let tokenRes = tool.checkToken(ctx);
  let pagesize = ctx.request.body.pagesize || 10
  let pageno = ctx.request.body.pageno || 0

  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {

    let searchRes = {
      errCode: 1,
      msg: 'error'
    }
    if (myblogsType === '0' || myblogsType === '1') {
      // console.log(myblogsType)
      // 查询article信息
      await mSearch.search_article_myblogs(myblogsType, user_id, searchText, searchDate, pagesize, pageno).then(async res => {
        // console.log(res)
        let data = res
        await mSearch.search_article_myblogs_total(myblogsType, user_id, searchText, searchDate).then(countRes => {
          if (countRes[0] && countRes[0]['count(article_id)'] > -1) {
            searchRes = {
              errCode: 0,
              data: data,
              total: countRes[0]['count(article_id)'],
              pagesize: pagesize,
              msg: 'success'
            }
          } else {
            searchRes = {
              errCode: 1,
              err: countRes,
              msg: 'total查询失败'
            }
          }
        }).catch(err => {
          searchRes[err] = err
        })
      }).catch(err => {
        searchRes[err] = err
      })
    } else if (myblogsType === '2') {
      // console.log(myblogsType)
      // 搜索file
      await mSearch.search_file_myblogs(user_id, searchText, searchDate, pagesize, pageno).then(async res => {
        // console.log(res)
        let data = res
        await mSearch.search_file_myblogs_total(user_id, searchText, searchDate).then(countRes => {
          if (countRes[0] && countRes[0]['count(*)'] > -1) {
            searchRes = {
              errCode: 0,
              data: data,
              total: countRes[0]['count(*)'],
              pagesize: pagesize,
              msg: 'success'
            }
          } else {
            searchRes = {
              errCode: 1,
              err: countRes,
              msg: 'total查询失败'
            }
          }
        }).catch(err => {
          console.log(err)
          searchRes[err] = err
        })
      }).catch(err => {
        console.log(err)

        searchRes[err] = err
      })
    }
    ctx.body = searchRes
  }
})

//查询用户
router.post('/search_user_name', async (ctx) => {
  let searchRes = {
    errCode: 1,
    msg: 'error'
  }
  let searchText = ctx.request.body.searchText
  await mSearch.search_user_name(searchText).then(res => {
    console.log(res)
    let lists = [];
    res.forEach(item => {
      lists.push({
        userId: item.id,
        userName: item.name,
        avatarUrl: item.avatar

      })
    })
    searchRes = {
      errCode: 0,
      data: lists,
      msg: 'success'
    };
  }).catch(err => {
    searchRes['err'] = err;
    searchRes['msg'] = '数据获取失败';

  })
  ctx.body = searchRes
})
// 全局搜索
router.post('/search_blog_global', async (ctx) => {
  let searchText = ctx.request.body.searchText
  let pagesize = ctx.request.body.pagesize || 10
  let pageno = ctx.request.body.pageno || 0
  let searchRes = {
    errCode: 1,
    msg: 'error'
  }
  await mSearch.search_blog_global(searchText, pagesize, pageno).then(async res => {
    // console.log(res)
    let data = res;
    await mSearch.search_blog_global_total(searchText).then(countRes => {
      console.log(countRes)
      if (countRes[0] && countRes[0]['count(*)'] > -1) {
        searchRes = {
          errCode: 0,
          data: data,
          total: countRes[0]['count(*)'],
          pagesize: pagesize,
          msg: 'success'
        }
      } else {
        searchRes = {
          errCode: 1,
          err: countRes,
          msg: 'total查询失败'
        }
      }
    }).catch(err => {
      console.log(err)
      searchRes[err] = err
    })

  }).catch(err => {
    console.log(err)
    searchRes[err] = err
  })
  ctx.body=searchRes
})
//  全局搜索文件
router.post('/search_file_global', async (ctx) => {
  let searchText = ctx.request.body.searchText
  let pagesize = ctx.request.body.pagesize || 10
  let pageno = ctx.request.body.pageno || 0
  let searchRes = {
    errCode: 1,
    msg: 'error'
  }
  await mSearch.search_file_global(searchText, pagesize, pageno).then(async res => {
    console.log(res)
    let data = res;
    await mSearch.search_file_global_total(searchText).then(countRes => {
      console.log(countRes)
      if (countRes[0] && countRes[0]['count(*)'] > -1) {
        searchRes = {
          errCode: 0,
          data: data,
          total: countRes[0]['count(*)'],
          pagesize: pagesize,
          msg: 'success'
        }
      } else {
        searchRes = {
          errCode: 1,
          err: countRes,
          msg: 'total查询失败'
        }
      }
    }).catch(err => {
      console.log(err)
      searchRes[err] = err
    })

  }).catch(err => {
    console.log(err)
    searchRes[err] = err
  })
  ctx.body=searchRes
})
// 要调用routes方法才行
module.exports = router.routes();