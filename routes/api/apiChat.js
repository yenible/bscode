/*
 * @Author: yenible
 * @Date: 2021-04-11 17:04:01
 * @LastEditors: yenible
 * @LastEditTime: 2021-04-12 23:32:58
 * @Description: xxx
 */
/*
 * @Author: yenible
 * @Date: 2021-03-30 21:32:02
 * @LastEditors: yenible
 * @LastEditTime: 2021-03-31 22:05:15
 * @Description: xxx
 */

// 引用koa-router
const Router = require('koa-router');
const mChat = require('../../models/model_chat')
const fs = require('fs');
const path = require('path');
const tool = require('../../lib/util/tool');
const crypto = require('crypto'); //导入加密模块
const jwt = require('jsonwebtoken'); //token生成插件
const {
  countReset
} = require('console');

// 实例化
const router = new Router();

router.post('/get_chat_message', async (ctx) => {
  let user_id_1 = ctx.request.body.user_id
  let user_id_2 = ctx.request.body.user_id_2
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    let queryRes = {
      errCode: 1,
      msg: 'error'
    }
    console.log(tokenRes)
    if (tokenRes.id * 1 !== user_id_1 * 1) {
      ctx.body = {
        errCode: 1,
        msg: '打开的聊天url错误，请不要乱开'
      }
      return;
    }
    //todo：要按roomid查询
    await mChat.get_chat_message(user_id_1, user_id_2).then(res => {
      // console.log(res)
      queryRes = {
        errCode: 0,
        msg: 'success',
        data: res
      }
    }).catch(err => {
      queryRes['err'] = err;
      queryRes['msg'] = '查询聊天数据出错';

    })
    // 判断一下data是否为0，假如为0需要创建新的聊天室
    ctx.body = queryRes
  }
})
router.post('/get_recipient_info', async (ctx) => {
  let username = ctx.request.body.user_name
  let queryRes = {
    errCode: 1,
    msg: 'error'
  }
  console.log(username)
  await mChat.get_recipient_info(username).then(res => {
    // console.log(res)
    if (res[0]) {
      queryRes = {
        errCode: 0,
        msg: 'success',
        data: res[0]
      }
    } else {
      console.log(err)
      queryRes['err'] = res;
      queryRes['msg'] = '获取聊天对象数据出错';
    }

  }).catch(err => {
    queryRes['err'] = err;
    queryRes['msg'] = '获取聊天对象数据出错';

  })
  ctx.body = queryRes
})

router.post('/get_chat_list', async (ctx) => {
  let user_id = ctx.request.body.user_id
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    let queryRes = {
      errCode: 1,
      msg: 'error'
    }

    // 分两次获取
    // 1、第一次获取当前用户的全部room_id的最新的聊天数据（可能是自己发的也可能是对面发的）
    // 2、第二次获取当前room_id 对面发的最新的聊天记录，主要目的是获取到对面的用户名
    let latestArr = [];
    let latestSendtArr = []
    await mChat.get_latest_chat_list(user_id).then(res => {
      // res[0]是最新的消息
      if (res && res.length === 0) {
        queryRes = {
          errCode: 0,
          msg: '无聊天信息',
          data: []
        }
      } else if (res && res) {
        latestArr = res
      } else {
        console.log(res)
        queryRes['err'] = res;
        queryRes['msg'] = '查询get_latest_chat_list出错';
      }
    }).catch(err => {
      console.log(err)
      queryRes['err'] = err;
      queryRes['msg'] = '查询get_latest_chat_list出错';

    })
    if (queryRes['err'] ||queryRes['errCode']===0) {
      ctx.body = queryRes
      return;
    }
    await mChat.get_latest_chat_send(user_id).then(res => {
      // console.log(res)
      if (res && res.length === 0) {
        queryRes = {
          errCode: 0,
          msg: 'success',
          data: latestArr
        }
      } else if (res && res.length > 0) {
        latestSendtArr = res
      } else {
        console.log(res)
        queryRes['err'] = res;
        queryRes['msg'] = '查询get_latest_chat_send出错';
      }
    }).catch(err => {
      console.log(err)
      queryRes['err'] = err;
      queryRes['msg'] = '查询get_latest_chat_send出错';

    })
    if (queryRes['err']||queryRes['errCode']===0) {
      ctx.body = queryRes
      return;
    }
    // 数组转对象
    let latestSendtObj={}
    for (var key in latestSendtArr) {
      latestSendtObj[latestSendtArr[key].chat_id] = latestSendtArr[key];
    }
    // 合并两个数组
    latestArr.forEach(item=>{
      if(latestSendtObj[item.chat_id]){
        item['receive_user_name'] = latestSendtObj[item.chat_id].receive_user_name
        item['receive_user_avatar'] = latestSendtObj[item.chat_id].receive_user_avatar
         
      }
    })
    queryRes = {
      errCode: 0,
      msg: 'success',
      data: latestArr
    }


    ctx.body = queryRes
  }

})

router.post('/remark_room', async (ctx) => {
  let room_id = ctx.request.body.room_id
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    let queryRes={
      errCode:1,
      msg:'error'
    }
    await mChat.remark_room(room_id).then(res => {
      // console.log(res)
      if (res.affectedRows*1>-1) {
        queryRes = {
          errCode: 0,
          msg: '已读成功'
        }
      } else {
        queryRes['err'] = res;
        queryRes['msg'] = '已读出错'
      }

    }).catch(err => {
      console.log(err)
      queryRes['err'] = err;
      queryRes['msg'] = '已读出错';

    })
    ctx.body =queryRes
  }

})

router.post('/clear_chat', async (ctx) => {
  let user_id_1 = ctx.request.body.user_id
  let user_id_2 = ctx.request.body.user_id_2
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    let delRes = {
      errCode:1,
      msg:'error'
    }
    await mChat.clear_chat_send(user_id_1,user_id_2).then(async res => {
      // console.log(res)
      if (res.affectedRows*1>-1) {
        await mChat.clear_chat_receive(user_id_1,user_id_2).then(async res2 => {
          // console.log(res2)
          if (res2.affectedRows*1>-1) {
            
            delRes = {
              errCode: 0,
              msg: '清空成功'
            }
          } else {
            delRes['err'] = res2;
            delRes['msg'] = 'clear_chat_receive清空出错'
          }
    
        }).catch(err => {
          console.log(err)
          delRes['err'] = err;
          delRes['msg'] = 'clear_chat_receive清空出错';
    
        })
        
      } else {
        delRes['err'] = res;
        delRes['msg'] = 'clear_chat_send清空出错'
      }

    }).catch(err => {
      console.log(err)
      delRes['err'] = err;
      delRes['msg'] = 'clear_chat_send清空出错';

    })
    ctx.body =delRes
    }

    })
// 查询获取用户信息
module.exports = router.routes();