/*
 * @Author: yenible
 * @Date: 2021-03-30 21:32:02
 * @LastEditors: yenible
 * @LastEditTime: 2021-05-04 10:52:16
 * @Description: xxx
 */

// 引用koa-router
const Router = require('koa-router');
const mAdmin = require('../../models/model_admin')
const mUser = require('../../models/model_user')
const fs = require('fs');
const path = require('path');
const tool = require('../../lib/util/tool');
const crypto = require('crypto'); //导入加密模块
const jwt = require('jsonwebtoken'); //token生成插件
const {
  countReset
} = require('console');

// 实例化
const router = new Router();

router.post('/login', async (ctx) => {
  let admin_name = ctx.request.body.admin_name
  let admin_password = ctx.request.body.admin_password

  let loginRes = {
    errCode: 1,
    msg: 'error'
  }
  let salt = '';
  // 查询账号，获取盐值
  await mAdmin.query_admin_name(admin_name).then(res => {
    console.log(res)
    if (res && res[0] && res[0]['admin_name']) {
      salt = res[0]['salt']
    } else {
      loginRes['err'] = res
      loginRes['msg'] = '账号不存在'
    }
  }).catch(err => {
    loginRes['err'] = err
    loginRes['msg'] = '账号名查询失败'
  })
  if (loginRes['err']) {
    ctx.body = loginRes;
    return;
  }
  // console.log(salt)
  let cryptoPassword = cryptPwd(admin_password, salt);

  // 验证
  await mAdmin.login(admin_name, cryptoPassword.password).then(res => {
    // console.log(res)
    if (res[0] && res[0].admin_id) {
      let token = generateToken({
        id: res[0].admin_id,
        name: res[0].admin_name,
        isAdmin: true
      });
      loginRes = {
        errCode: 0,
        msg: '登录成功',
        data: {
          token: token,
          id: res[0].admin_id
        }

      }
    } else {
      loginRes['msg'] = '账户或密码错误'
      loginRes['err'] = res
    }
  }).catch(err => {
    loginRes['msg'] = '账户或密码错误'
    loginRes['err'] = err

  })
  ctx.body = loginRes
})
//生成token的方法
function generateToken(data) {
  let created = Math.floor(Date.now() / 1000);
  let cert = fs.readFileSync(path.join(__dirname, '../../config/key/rsa_private_key.pem')); //私钥
  let token = jwt.sign({
    data,
    exp: created + 3600 * 24
  }, cert, {
    algorithm: 'RS256'
  });
  return token;
}
router.post('/get_user_list', async (ctx) => {
  let email = ctx.request.body.email || ''
  let name = ctx.request.body.name || ''
  let pagesize = ctx.request.body.pagesize || 10
  let pageno = ctx.request.body.pageno || 0
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    let queryRes = {
      errCode: 1,
      msg: 'error'
    }
    await mAdmin.get_user_list(email, name, pageno, pagesize).then(async res => {
      // console.log(res);
      await mAdmin.get_user_list_total(email, name).then(conutRes => {
        // console.log(conutRes)
        if (conutRes[0] && conutRes[0]['count(*)'] >= 0) {
          queryRes = {
            errCode: 0,
            msg: 'success',
            data: res,
            total: conutRes[0]['count(*)']
          }
        } else {
          // console.log(res)
          queryRes['msg'] = '获取用户总数失败';
          queryRes['err'] = res;
        }
      }).catch(err => {
        console.log(err)
        queryRes['msg'] = '获取用户总数失败';
        queryRes['err'] = err;
      })

    }).catch(err => {
      queryRes['msg'] = '获取账户信息错误'
      queryRes['err'] = err
    })
    ctx.body = queryRes
  }
})

router.post('/update_user_info', async (ctx) => {
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    let email = ctx.request.body.email
    let name = ctx.request.body.name
    let score = ctx.request.body.score
    let article_amount = ctx.request.body.article_amount
    let file_amount = ctx.request.body.file_amount
    let id = ctx.request.body.id
    let updateObj = {
      email: email,
      name: name,
      INT_score: score,
      INT_article_amount: article_amount,
      INT_file_amount: file_amount,
    }
    let queryRes = {
      msg: 'err',
      errCode: 1
    };
    // await mUser.find_user({
    await mAdmin.update_user(updateObj, id).then(res => {
      if (res.affectedRows*1 === 1) {
        queryRes = {
          msg: 'success',
          errCode: 0
        };
      } else {
        queryRes = {
          msg: '修改数据失败',
          errCode: 1,
          err: res
        };
      }

    }).catch(err => {
      console.log(err);
      queryRes = {
        err: err,
        errCode: 1,
        msg: '更新数据失败'
      };
    })
    ctx.body = queryRes
  }
})


router.post('/delete_user', async (ctx) => {
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    let id = ctx.request.body.id
    let queryRes = {
      msg: 'err',
      errCode: 1
    };
    await mAdmin.delete_user(id).then(res => {
      if (res.affectedRows*1 === 1) {
        queryRes = {
          msg: '删除成功',
          errCode: 0
        };
      } else {
        queryRes = {
          msg: '删除失败',
          errCode: 1,
          err: res
        };
      }

    }).catch(err => {
      console.log(err);
      queryRes = {
        err: err,
        errCode: 1,
        msg: '删除失败'
      };
    })
    ctx.body = queryRes
  }
})

router.post('/search_blog_admin', async (ctx) => {
  let title = ctx.request.body.title || ''
  let text = ctx.request.body.text || ''
  let authName = ctx.request.body.authName || ''
  let pagesize = ctx.request.body.pagesize || 10
  let pageno = ctx.request.body.pageno || 0
  let searchRes = {
    errCode: 1,
    msg: 'error'
  }
  await mAdmin.search_blog_admin(authName, title, text, pagesize, pageno).then(async res => {
    // console.log(res)
    let data = res;
    await mAdmin.search_blog_admin_total(authName, title, text).then(countRes => {
      // console.log(countRes)
      if (countRes[0] && countRes[0]['count(*)'] > -1) {
        searchRes = {
          errCode: 0,
          data: data,
          total: countRes[0]['count(*)'],
          pagesize: pagesize,
          msg: 'success'
        }
      } else {
        searchRes = {
          errCode: 1,
          err: countRes,
          msg: 'total查询失败'
        }
      }
    }).catch(err => {
      console.log(err)
      searchRes['err'] = err
    })

  }).catch(err => {
    console.log(err)
    searchRes['err'] = err
  })
  ctx.body = searchRes
})


router.post('/search_file_admin', async (ctx) => {
  let title = ctx.request.body.title || ''
  let text = ctx.request.body.text || ''
  let authName = ctx.request.body.authName || ''
  let pagesize = ctx.request.body.pagesize || 10
  let pageno = ctx.request.body.pageno || 0
  let searchRes = {
    errCode: 1,
    msg: 'error'
  }
  await mAdmin.search_file_admin(authName, title, text, pagesize, pageno).then(async res => {
    // console.log(res)
    let data = res;
    await mAdmin.search_file_admin_total(authName, title, text).then(countRes => {
      // console.log(countRes)
      if (countRes[0] && countRes[0]['count(*)'] > -1) {
        searchRes = {
          errCode: 0,
          data: data,
          total: countRes[0]['count(*)'],
          pagesize: pagesize,
          msg: 'success'
        }
      } else {
        searchRes = {
          errCode: 1,
          err: countRes,
          msg: 'total查询失败'
        }
      }
    }).catch(err => {
      console.log(err)
      searchRes['err'] = err
    })

  }).catch(err => {
    console.log(err)
    searchRes['err'] = err
  })
  ctx.body = searchRes
})
// 
router.post('/get_suggest', async (ctx) => {
  let title = ctx.request.body.title || ''
  let content = ctx.request.body.content || ''
  let suggest_state = ctx.request.body.suggest_state || 0
  let pageno = ctx.request.body.pageno || 0
  let pagesize = ctx.request.body.pagesize || 10
  let searchRes = {
    errCode: 1,
    msg: 'error'
  }
  await mAdmin.get_suggest(title, content, suggest_state, pagesize, pageno).then(async res => {
    // console.log(res)
    await mAdmin.get_suggest_total(title, content, suggest_state, pagesize, pageno).then(async countRes => {
      console.log(countRes)
      if (countRes && countRes[0] && countRes[0]['count(*)'] * 1 > -1) {
        searchRes = {
          errCode: 0,
          msg: 'success',
          data: res,
          total: countRes[0]['count(*)']
        }
      }

    }).catch(err => {
      console.log(err)
      searchRes['err'] = err
      searchRes['msg'] = 'get_suggest_total失败'
    })

  }).catch(err => {
    console.log(err)
    searchRes['err'] = err
    searchRes['msg'] = 'get_suggest失败'
  })
  ctx.body = searchRes
})
router.post('/reply_suggest', async (ctx) => {
  let suggest_id = ctx.request.body.suggest_id
  let content = ctx.request.body.content
  let admin_id = ctx.request.body.admin_id
  let reply_time = tool.format(new Date());

  let searchRes = {
    errCode: 1,
    msg: 'error'
  }
  await mAdmin.reply_suggest(suggest_id, content, admin_id, reply_time).then(async res => {
    console.log(res)
    if (res.affectedRows * 1 >= 1) {
      searchRes = {
        errCode: 0,
        msg: '回复成功'
      }
    } else {
      searchRes['err'] = res
      searchRes['msg'] = 'reply_suggest失败'
    }

  }).catch(err => {
    console.log(err)
    searchRes['err'] = err
    searchRes['msg'] = 'reply_suggest失败'
  })
  ctx.body = searchRes
})

router.post('/get_admin_list', async (ctx) => {
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    let queryRes = {
      errCode: 1,
      msg: 'error'
    }
    // 获取管理员账号
    await mAdmin.get_admin_list().then(res => {
      // console.log(res)
      if (res && res[0]) {
        queryRes = {
          errCode: 0,
          msg: 'success',
          data: res
        }
      } else {
        queryRes['err'] = res
        queryRes['msg'] = '获取管理员账号错误1'

      }
    }).catch(err => {
      queryRes['msg'] = '获取管理员账号错误2'
      queryRes['err'] = res

    })
    ctx.body = queryRes
  }
})



router.post('/create_admin', async (ctx) => {
  let name = ctx.request.body.name
  let level = ctx.request.body.level
  // 获取加密后的密码
  let cryptObj = cryptPwd(ctx.request.body.password);
  let tokenRes = tool.checkToken(ctx);
  let createRes = {
    errCode: 1,
    msg: 'error'
  }
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    // 账号名查重
    let isExist = false;
    await mAdmin.query_admin_name(name).then(res => {
      console.log(res)
      if (res && res[0] && res[0]['admin_name']) {
        isExist = true;
        createRes = {
          errCode: 1,
          msg: '账号名已存在'
        }
      }
    }).catch(err => {
      createRes['err'] = err
      createRes['msg'] = '账号名查重失败'
    })
    if (createRes['err'] || isExist) {
      ctx.body = createRes;
      return;
    }
    await mAdmin.create_admin(name, cryptObj.password, level, cryptObj.salt).then(res => {
      // console.log(res)
      if (res.affectedRows * 1 >= 1) {
        createRes = {
          errCode: 0,
          msg: '创建管理员账号成功'
        }
      } else {
        createRes['err'] = res
        createRes['msg'] = '创建管理员账号失败'
      }
    }).catch(err => {
      createRes['msg'] = '创建管理员账号失败'
      createRes['err'] = res

    })
    ctx.body = createRes
  }

})

// 获取随机的盐值
function getRandomSalt() {
  return Math.random().toString().slice(2, 10);
}
// 密码加密，加盐加密
function cryptPwd(password, salt) {
  if (!salt) {
    salt = getRandomSalt();
  }
  let saltPassword = password + ':' + salt;
  // dm5加密
  let md5 = crypto.createHash('md5');
  let result = md5.update(saltPassword).digest('hex');
  console.log('salt:'+salt)
  console.log('old:'+password)
  console.log('new:'+result)
  return {
    password: result,
    salt: salt
  };
}



router.post('/delete_admin', async (ctx) => {
  let id = ctx.request.body.id
  let tokenRes = tool.checkToken(ctx);

  let deleteRes = {
    errCode: 1,
    msg: 'error'
  }
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    await mAdmin.delete_admin(id).then(res => {
      console.log(res)
      if (res.affectedRows * 1 === 1) {
        deleteRes = {
          errCode: 0,
          msg: '删除管理员账号成功'
        }
      } else {
        deleteRes['err'] = res
        deleteRes['msg'] = '删除管理员账号失败'
      }
    }).catch(err => {
      console.log(err)
      deleteRes['msg'] = '删除管理员账号失败'
      deleteRes['err'] = err

    })
    ctx.body = deleteRes
  }
})

router.post('/update_admin_password', async (ctx) => {
  let admin_password = ctx.request.body.password
  let admin_name = ctx.request.body.name
  let tokenRes = tool.checkToken(ctx);

 
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    let updateRes = {
      errCode: 1,
      msg: 'error'
    }
    let salt = '';
    // 查询账号，获取盐值
    await mAdmin.query_admin_name(admin_name).then(res => {
      console.log(res)
      if (res && res[0] && res[0]['admin_name']) {
        salt = res[0]['salt']
      } else {
        updateRes['err'] = res
        updateRes['msg'] = '账号不存在'
      }
    }).catch(err => {
      updateRes['err'] = err
      updateRes['msg'] = '账号名查询失败'
    })
    if (updateRes['err']) {
      ctx.body = updateRes;
      return;
    }
    console.log(salt)
    let cryptoPassword = cryptPwd(admin_password, salt);
    await mAdmin.update_admin_password(admin_name,cryptoPassword['password']).then(res => {
      console.log(res)
      if (res.affectedRows*1 === 1) {
        updateRes = {
          msg: '密码修改成功',
          errCode: 0
        }
      } else {
        updateRes['err'] = res
        updateRes['msg'] = '密码修改失败'
      }      
    }).catch(err => {
      updateRes['err'] = err
      updateRes['msg'] = '密码修改失败'
    })

    ctx.body = updateRes;
    
  }
})
module.exports = router.routes();