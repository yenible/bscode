/*
 * @Author: yenible
 * @Date: 2021-03-12 20:53:25
 * @LastEditors: yenible
 * @LastEditTime: 2021-04-17 22:43:15
 * @Description: xxx
 */

// 引用koa-router
const Router = require('koa-router');
const mComment = require('../../models/model_comment')
const fs = require('fs');
const path = require('path');
const tool = require('../../lib/util/tool');
const {
  delete_sons_comment
} = require('../../models/model_comment');

// 实例化
const router = new Router();

//保存文章的评论
router.post('/save_article_comment', async (ctx) => {
  let comment_content = ctx.request.body.comment
  let thread = ctx.request.body.thread || '/'
  let INT_depth = ctx.request.body.depth || -1;
  let INT_parent_id = ctx.request.body.parent_id || -1
  let article_id = ctx.request.body.article_id
  let INT_user_id = ctx.request.body.user_id
  let create_time = tool.format(new Date());


  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {

    let saveRes = {
      errCode: 1,
      msg: 'error'
    }
    await mComment.save_article_comment({
      comment_content,
      thread,
      INT_depth,
      INT_parent_id,
      article_id,
      INT_user_id,
      create_time
    }).then(
      res => {
        // console.log(res)
        if (res.affectedRows >= 1) {
          saveRes = {
            errCode: 0,
            msg: 'success'
          }
        } else {
          saveRes['msg'] = res
        }
      }).catch(err => {
      console.log(err)
      saveRes['err'] = err
    })
    if (!saveRes['err']) {
      // 获取文章作者id
      let message_user = -1 ;
      console.log(INT_depth)
      if(INT_depth*1!==0){
        // 查找parent_id的user
        await mComment.query_article_parent_comment(INT_parent_id).then(res=>{
          if (res[0] && res[0].user_id) {
             message_user = res[0].user_id
          }else{
            saveRes = {
              errCode: 1,
              msg: 'parent_id查找出错',
            }
            message_user=-1
          }
        })
      }else{
        await mComment.query_article_auth(article_id).then(async res => {
          console.log(res)
          if (res[0] && res[0].user_id) {
            message_user = res[0].user_id
          } else {
            saveRes = {
              errCode: 1,
              msg: 'article查找出错',
              err: res
            }
          }
        }).catch(err => {
          console.log(err)
          saveRes = {
            errCode: 1,
            msg: 'article查找出错',
            err: err
          }
        })
      }
      if(message_user===-1){
        ctx.body=saveRes;
        return;
      }
        

      await mComment.save_message_file({
        INT_user_id: message_user,
        INT_reply_id: INT_user_id,
        reply_time: create_time,
        reply_type: 'article',
        article_id: article_id,
        reply_content:comment_content
      }).then(msgRes => {
        console.log(msgRes)
      }).catch(err => {
        console.log(err)

        saveRes = {
          errCode: 1,
          msg: '保存message错误,article查询不到userid',
        }
      })
    }
    ctx.body = saveRes;
  }
})

// 查询文章全部评论
router.post('/query_article_all_comment', async (ctx) => {
  let article_id = ctx.request.body.article_id
  let queRes = {
    errCode: 1,
    msg: 'error'
  }

  await mComment.query_article_all_comment(article_id).then(
    res => {
      // console.log(1111111111111111111111111111111111111111111111111111111111111111111111)
      // console.log(res)
      // console.log(1111111111111111111111111111111111111111111111111111111111111111111111)

      // 重组数据
      // 1、按照深度depth进行筛选，同上得depthObj
      let depthObj = {};
      let maxDepth = 0;
      res.forEach(item => {
        // 获取最大深度值
        if (item.depth > maxDepth) {
          maxDepth = item.depth
        }

        if (depthObj[item.depth]) {
          depthObj[item.depth][item.comment_id] = item;
        } else {
          let o = {}
          o[item.comment_id] = item
          depthObj[item.depth] = o
        }
      });
      // console.log(depthObj);
      for (let i = maxDepth; i > 0; i--) {
        for (let key in depthObj[i]) {
          // console.log(i - 1)
          // console.log(depthObj[i - 1])
          // console.log(depthObj[i][key].parent_id)
          // console.log(depthObj[i - 1][depthObj[i][key].parent_id])
          // console.log( '12312')
          if (depthObj[i - 1][depthObj[i][key].parent_id]['childList']) {
            depthObj[i][key]['parent_name'] = depthObj[i - 1][depthObj[i][key].parent_id]['name']
            depthObj[i - 1][depthObj[i][key].parent_id]['childList'].push(depthObj[i][key])
          } else {
            depthObj[i][key]['parent_name'] = depthObj[i - 1][depthObj[i][key].parent_id]['name']
            depthObj[i - 1][depthObj[i][key].parent_id]['childList'] = [depthObj[i][key]]
          }
          depthObj[i - 1][depthObj[i][key].parent_id]['childList'].sort(function (b, a) {
            return a.create_time - b.create_time
          })
        }
      }
      // console.log(depthObj);
      let data = depthObj['0'] ? Object.values(depthObj['0']).sort(function (b, a) {
        return a.create_time - b.create_time
      }) : []
      queRes = {
        data: data,
        errCode: 0,
        msg: 'success'
      }
    }).catch(err => {
    console.log(err)
    queRes['err'] = err
  })
  ctx.body = queRes
})


//保存文件的评论
router.post('/save_file_comment', async (ctx) => {
  let comment_content = ctx.request.body.comment
  let thread = ctx.request.body.thread || '/'
  let INT_depth = ctx.request.body.depth || -1;
  let INT_parent_id = ctx.request.body.parent_id || -1
  let INT_file_id = ctx.request.body.file_id
  let INT_user_id = ctx.request.body.user_id
  let create_time = tool.format(new Date());


  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {

    let saveRes = {
      errCode: 1,
      msg: 'error'
    }
    await mComment.save_file_comment({
      comment_content,
      thread,
      INT_depth,
      INT_parent_id,
      INT_file_id,
      INT_user_id,
      create_time
    }).then(
      res => {
        // console.log(res)
        if (res.affectedRows >= 1) {
          saveRes = {
            errCode: 0,
            msg: 'success'
          }
        } else {
          saveRes['msg'] = res
        }
      }).catch(err => {
      console.log(err)
      saveRes['err'] = err
    })
    if (!saveRes['err']) {
      // 获取文章作者id
      let message_user = -1 ;
      console.log(INT_depth)
      console.log(INT_depth*1!==0)
      console.log(INT_depth!==0)
      if(INT_depth*1!==0){
        // 查找parent_id的user
        await mComment.query_file_parent_comment(INT_parent_id).then(res=>{
          if (res[0] && res[0].user_id) {
             message_user = res[0].user_id
          }else{
            saveRes = {
              errCode: 1,
              msg: 'parent_id查找出错',
            }
            message_user=-1
          }
        })
      }else{
        await mComment.query_file_auth(INT_file_id).then(async res => {
          console.log(res)
          if (res[0] && res[0].user_id) {
            message_user = res[0].user_id
          } else {
            saveRes = {
              errCode: 1,
              msg: 'file查找出错',
              err: res
            }
          }
        }).catch(err => {
          console.log(err)
          saveRes = {
            errCode: 1,
            msg: 'file查找出错',
            err: err
          }
        })
      }
      if(message_user===-1){
        ctx.body=saveRes;
        return;
      }
        

      await mComment.save_message_file({
        INT_user_id: message_user,
        INT_reply_id: INT_user_id,
        reply_time: create_time,
        reply_type: 'file',
        file_id: INT_file_id,
        reply_content:comment_content
      }).then(msgRes => {
        console.log(msgRes)
      }).catch(err => {
        console.log(err)

        saveRes = {
          errCode: 1,
          msg: '保存message错误,article查询不到userid',
        }
      })
    }
    ctx.body = saveRes;
  }
})

// 查询文章全部评论
router.post('/query_file_all_comment', async (ctx) => {
  let file_id = ctx.request.body.file_id
  let queRes = {
    errCode: 1,
    msg: 'error'
  }

  await mComment.query_file_all_comment(file_id).then(
    res => {
      // console.log(res)
      // 重组数据 
      // 1、按照深度depth进行筛选，同上得depthObj
      let depthObj = {};
      let maxDepth = 0;
      res.forEach(item => {
        if (item.depth > maxDepth) {
          maxDepth = item.depth
        }

        if (depthObj[item.depth]) {
          depthObj[item.depth][item.comment_id] = item;
        } else {
          let o = {}
          o[item.comment_id] = item
          depthObj[item.depth] = o
        }
      });
      // console.log(depthObj);
      for (let i = maxDepth; i > 0; i--) {
        for (let key in depthObj[i]) {
      //     console.log(1111111111111111111111111111111111111111)
      // console.log(depthObj[i - 1][depthObj[i][key].parent_id]);
      // console.log(depthObj[i - 1][depthObj[i][key].parent_id]['childList']);
      // console.log(1111111111111111111111111111111111111111)

          if (depthObj[i - 1][depthObj[i][key].parent_id]['childList']) {
            depthObj[i - 1][depthObj[i][key].parent_id]['childList'].push(depthObj[i][key])
          } else {
            depthObj[i - 1][depthObj[i][key].parent_id]['childList'] = [depthObj[i][key]]
          }
          depthObj[i - 1][depthObj[i][key].parent_id]['childList'].sort(function (b, a) {
            return a.create_time - b.create_time
          })
        }
      }
      // console.log(depthObj);
      let data = depthObj['0'] ? Object.values(depthObj['0']).sort(function (b, a) {
        return a.create_time - b.create_time
      }) : []
      queRes = {
        data: data,
        errCode: 0,
        msg: 'success'
      }
    }).catch(err => {
    console.log(err)
    queRes['err'] = err
  })
  ctx.body = queRes
})

// 删除评论及其子评论
router.post('/delete_comment', async (ctx) => {
  let comment_id = ctx.request.body.comment_id
  let type = ctx.request.body.type
   
  let delRes = {
    errCode: 1,
    msg: 'error'
  }
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    if(type==='file'){

    // 1、删除thread带有comment_id的全部子评论
    await mComment.delete_sons_comment(comment_id).then(res => {}).catch(err => {
      delRes['err'] = err;
      delRes['msg'] = '删除资源子评论出错'
    })
    if (delRes['err']) {
      ctx.body = delRes;
      return;
    }
    // 2、删除comment_id评论
    await mComment.delete_comment(comment_id).then(res => {
      if (res.affectedRows === 1) {
        delRes = {
          errCode: 0,
          msg: '资源评论删除成功'
        }
      } else {
        delRes['err'] = res;
        delRes['msg'] = '111删除资源评论出错'
      }
    }).catch(err => {
      delRes['err'] = err;
      delRes['msg'] = '222删除资源评论出错'
    })
  }else{
        // 1、删除thread带有comment_id的全部子评论
        await mComment.delete_sons_comment_article(comment_id).then(res => {}).catch(err => {
          delRes['err'] = err;
          delRes['msg'] = '删除文章子评论出错'
        })
        if (delRes['err']) {
          ctx.body = delRes;
          return;
        }
        // 2、删除comment_id评论
        await mComment.delete_comment_article(comment_id).then(res => {
          if (res.affectedRows === 1) {
            delRes = {
              errCode: 0,
              msg: '文章评论删除成功'
            }
          } else {
            delRes['err'] = res;
            delRes['msg'] = '111删除文章评论出错'
          }
        }).catch(err => {
          delRes['err'] = err;
          delRes['msg'] = '222删除文章评论出错'
        })
  }
    ctx.body = delRes;
  }
})

// 要调用routes方法才行
module.exports = router.routes();