/*
 * @Author: yenible
 * @Date: 2021-01-26 16:14:53
 * @LastEditors: yenible
 * @LastEditTime: 2021-04-17 10:44:14
 * @Description: 图片、文件上传的接口文件
 */
const Router = require('koa-router');
const fs = require('fs');
const path = require('path');
const send = require('koa-send');
const config = require('../../config/my_config')
const article = require('../../models/model_article')
const mUser = require('../../models/model_user')
const mFile = require('../../models/model_file')
const deleteFolderRecursive = require('../../lib/util/deleteFolderRecursive')
// const tool = require('../../lib/util/tool');

// 图片静态服务器域名地址
const imgURL = config.blogImgURL
const avatarImgUrl = config.avatarImgUrl;
// 图片本机地址
const router = new Router();
const articleImgSrc = config.articleImgSrc;
const avatarImgSrc = config.avatarImgSrc;
const downloadTimeRange = config.downloadTimeRange;
//文件上传
let formidable = require("formidable");
const tool = require('../../lib/util/tool');
router.post('/uploadImg', async (ctx, next) => {
  let path = config.imgLocalSrc;
  // console.log(config)
  var form = new formidable();
  form.uploadDir = path;
  form.keepExtensions = true;
  form.multiples = true;
  let res = {
    errCode: 1,
    msg: '未执行parse'
  }
  // console.log('aaaaa');
  await upImg().then(res => {
    // console.log(111)
    ctx.body = res;
  }).catch(err => {
    ctx.body = err
  });

  function upImg() {
    return new Promise((resolve, reject) => {
      form.parse(ctx.req, async function (err, fields, files) {
        if (files.modal_file) {
          res = await rename(files.modal_file.path, files.modal_file.name, 'preview');
          resolve(res)
        } else {
          res = {
            errCode: 1,
            exist: 1,
            msg: "文件上传失败，modal_file不存在 ",
            // fileUrl: 'path + '/' + _new'
          }
          reject(res)
        }

      })
    })
  }

  async function rename(oldpath, _new, code, bId) {
    // console.log(fs.existsSync(path))
    let isFloderExist = await fs.existsSync(path)
    // console.log(3);

    // 确定保存的路径文件夹存在，否则创建对于文件夹路径
    if (!isFloderExist) {
      await fs.mkdir(path, (err) => {
        if (err) {
          throw err;
        } else {
          console.log('ok!');
        }
      })
      console.log('创建文件夹！')
    }
    let isNewFileExist = await fs.existsSync(path + '/' + _new);
    // console.log(imgURL + '/' + _new)
    let isOldFileExist = await fs.existsSync(oldpath);

    // 文件未上传，同时oldpath不存在，则报错
    if (!isOldFileExist) {
      return {
        errCode: 1,
        exist: 1,
        msg: "文件上传失败，文件重命名失败，找不到 " + oldpath + '文件',
        // fileUrl: 'path + '/' + _new'
      }
    }
    let res;
    try {
      await fs.renameSync(oldpath, path + '/' + _new)
      return {
        errCode: 0,
        exist: 0,
        msg: "文件已经上传成功",
        // fileUrl: imgURL + '/' + _new,
        filepath: path + '/' + _new,
        fileName: _new
      };
    } catch (err) {
      return {
        errCode: 1,
        msg: err
      }
    }
  }

});

// 移动文章图片文件位置，将文件数据写入数据库图片-文章表
router.post('/move_article_img', async (ctx, next) => {
  // let file = await fs.createReadStream(ctx.request.body.imgSrc);
  let oldfile = ctx.request.body.imgSrc;
  let articleSrc = articleImgSrc + "/" + ctx.request.body.articleid;
  // 确定文件夹路径是否存在
  let isFloderExist = await fs.existsSync(articleSrc)
  let moveRes = {
    errCode: 1,
    msg: 'error'
  }
  // console.log(3);

  // 确定保存的路径文件夹存在，否则创建对于文件夹路径
  if (!isFloderExist) {
    await fs.mkdir(articleSrc, {
      recursive: true
    }, (err) => {
      if (err) {
        throw err;
      } else {
        console.log('ok!');
      }
    })
    console.log('创建文件夹！')
  }
  // 把对应文章图片放到对应的文章id的文件夹下
  let newfile = articleImgSrc + "/" + ctx.request.body.articleid + "/" + ctx.request.body.imgName
  console.log(newfile)
  // try {
  // // 1、复制,使用pipe管道
  // let out = await fs.createWriteStream(newfile);
  // await file.pipe(out);

  await new Promise((resolve, reject) => {
    // 复制使用copyFile
    fs.copyFile(oldfile, newfile, function (err) {
      if (err) {
        moveRes = {
          errCode: 1,
          msg: 'copyFile错误',
          err:err
        };
        reject(err)
      } else{
        resolve(console.log('move成功'))
      };
    })
  }).then(res=>{
    // console.log(res)
  })

  if(moveRes['err']){
    ctx.body=moveRes
    return;
  }


  // 确认文件是否成功转移且存在】
  let ismoveFile = await fs.existsSync(newfile);
  // 删除缓存区的文件？后期作个定时任务定时删除
  // deleteFolderRecursive(newfile)
  try {
    await fs.unlinkSync(oldfile);
  } catch (err) {
    console.log(err);
    moveRes = {
      errCode: 1,
      msg: err
    };
  }
  if(moveRes['err']){
    ctx.body=moveRes
    return;
  }
  // 写入数据库存储,需要获取图片名称以及文章id
  let dbData = {
    img_name: ctx.request.body.imgName,
    article_id: ctx.request.body.articleid,
    img_src: newfile
  }
  if (ismoveFile) {
    await article.add_img(dbData).then(res => {
      console.log(res)
    }).catch(err => {
      console.log(err)
    })
    moveRes = {
      errCode: 0,
      isMove: 0,
      msg: "文件转移成功",
      fileUrl: imgURL + "/" + ctx.request.body.articleid + "/" + ctx.request.body.imgName
    };
  } else {
    moveRes = {
      errCode: 0,
      isMove: 1,
      msg: "文件转移失败111"
    };
  }
  ctx.body = moveRes

});

router.post('/move_avatar', async (ctx, next) => {
  // let file = fs.createReadStream(ctx.request.body.imgSrc);
  let oldfile = ctx.request.body.imgSrc;

  let userid = ctx.request.body.userid
  let avatarSrc = avatarImgSrc + "/" + userid;
  // 确定文件夹路径是否存在
  let isFloderExist = await fs.existsSync(avatarSrc)
  // console.log(3);
  let moveRes = {
    errCode: 1,
    msg: 'error'
  }
  // 确定保存的路径文件夹存在，否则创建对于文件夹路径
  if (!isFloderExist) {
    await fs.mkdir(avatarSrc, {
      recursive: true
    }, (err) => {
      if (err) {
        throw err;
      } else {
        console.log('ok!');
      }
    })
    console.log('创建文件夹！')
  }
  // 把对应文章图片放到对应的文章id的文件夹下
  let newfile = avatarImgSrc + "/" + userid + "." + ctx.request.body.imgType
  // console.log(newfile)
  // 复制图片
  await new Promise((resolve, reject) => {
    // 复制使用copyFile
    fs.copyFile(oldfile, newfile, function (err) {
      if (err) {
        moveRes = {
          errCode: 1,
          msg: 'copyFile错误',
          err:err
        };
        reject(err)
      } else{
        resolve(console.log('move成功'))
      };
    })
  }).then(res=>{
    // console.log(res)
  })
  if(moveRes['err']){
    ctx.body=moveRes
    return;
  }
  // try {
  //   let out = fs.createWriteStream(newfile);
  //   // ./public/uploads/123/111.jpg
  //   file.pipe(out);
  // } catch (err) {
  //   ctx.body = {
  //     errCode: 1,
  //     msg: err
  //   };
  // }
  // 确认文件是否成功转移且存在】
  let ismoveFile = await fs.existsSync(newfile);
  console.log(ismoveFile)

  // 删除缓存区的文件？后期作个定时任务定时删除
  // deleteFolderRecursive(newfile)
  try {
    await fs.unlinkSync(ctx.request.body.imgSrc);
  } catch (err) {
    console.log(err);
    ctx.body = {
      errCode: 1,
      msg: err
    };
  }
  if(moveRes['err']){
    ctx.body=moveRes
    return;
  }
  let avatarUrl = avatarImgUrl + "/" + userid + "." + ctx.request.body.imgType
  console.log(avatarUrl)
  // 写入数据库存储,需要获取图片名称以及文章id
  await mUser.add_avatar(userid, avatarUrl).then(res => {
    console.log(res)
  }).catch(err => {
    console.log(err)
  })
  if (ismoveFile) {
    moveRes = {
      errCode: 0,
      isMove: 0,
      msg: "文件转移成功",
    };
  } else {
    moveRes = {
      errCode: 0,
      isMove: 1,
      msg: "文件转移失败222"
    };
  }
  ctx.body = moveRes
});
// router.get('/test', async (ctx) => {
//   ctx.body = {
//     filename: 123
//   }
// });

router.post('/upload_file', async (ctx, next) => {
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    let configPath = config.fileSrc;
    // console.log(config)
    var form = new formidable();
    form.uploadDir = configPath;
    form.keepExtensions = true;
    form.multiples = true;
    let res = {
      errCode: 1,
      msg: '未执行parse'
    }
    ctx.body = {
      id: ctx.request.body.user_id,

    }

    await upload().then(res => {
      // console.log(111)
      ctx.body = res;
    }).catch(err => {
      ctx.body = err
    });

    function upload() {
      return new Promise((resolve, reject) => {
        form.parse(ctx.req, async function (err, fields, files) {
          let userid = fields.user_id
          let file_title = fields.file_title
          let file_desc = fields.file_desc
          let file_score = fields.file_score
          let time = new Date()
          if (files.modal_file) {
            // 判断是否存在指定文件夹
            let isFloderExist = await fs.existsSync(configPath + userid)
            // 确定保存的路径文件夹存在，否则创建对于文件夹路径
            if (!isFloderExist) {
              await fs.mkdir(configPath + userid, (err) => {
                if (err) {
                  throw err;
                } else {
                  console.log('ok!');
                }
              })
              console.log('创建文件夹！')
            }
            try {
              await fs.renameSync(files.modal_file.path, configPath + userid + '/' + time.getTime() + files.modal_file.name);
              // 判断是否保存成功
              let isNewFloderExist = await fs.existsSync(configPath + userid + '/' + time.getTime() + files.modal_file.name)
              if (isNewFloderExist) {
                // 存入数据库
                await mFile.save_file({
                  file_title: file_title,
                  file_desc: file_desc,
                  INT_file_score: file_score,
                  upload_time: tool.format(time),
                  file_src: configPath + userid + '/' + time.getTime() + files.modal_file.name,
                  INT_user_id: userid,
                  file_name: files.modal_file.name
                }).then(result => {
                  // if(result)
                  res = {
                    errCode: 0,
                    msg: '资源保存成功',

                  }
                }).catch(err => {
                  res = {
                    errCode: 1,
                    msg: '文件信息保存数据库失败',
                    err: err
                  }
                })
                await mFile.update_user_file_amount(userid).then(updateRes => {
                  if (updateRes.affectedRows !== 1) {
                    res = {
                      errCode: 1,
                      msg: '用户file-amount更新失败',
                      err: err
                    }
                  }
                }).catch(err => {
                  res = {
                    errCode: 1,
                    msg: '用户file-amount更新失败',
                    err: err

                  }
                })
              } else {
                res = {
                  errCode: 1,
                  msg: '文件保存失败，renameSync错误',

                }
              }
            } catch (err) {
              res = {
                errCode: 1,
                msg: 'error',
                err: err
              }
              console.log(err)
            }
            resolve(res)
          } else {
            res = {
              errCode: 1,
              exist: 1,
              msg: "文件上传失败，modal_file不存在 ",
              // fileUrl: 'path + '/' + _new'
            }
            reject(res)
          }

        })
      })
    }
  }
});

router.post('/query_file_list', async (ctx, next) => {
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {

    let queRes = {
      errCode: 1,
      msg: 'error'
    }
    let pagesize = ctx.request.body.pagesize || 3
    let pageno = ctx.request.body.pageno || 0
    let user_id = ctx.request.body.user_id;
    // console.log(user_id)
    let params = ['file_desc', 'bower_amount', 'collect_amount', 'file_id', 'file_score', 'file_title', 'like_amount', 'upload_time', 'user_id']
    // 查询数据库信息
    await mFile.query_file_list(user_id, params, pageno, pagesize).then(async res => {
      // console.log(res)
      let data = res
      await mFile.query_file_list_total(user_id).then(countRes => {
        if (countRes[0] && countRes[0]['count(*)'] > -1) {
          queRes = {
            errCode: 0,
            data: data,
            total: countRes[0]['count(*)'],
            msg: '数据获取成功'
          }
        } else {
          queRes = {
            errCode: 1,
            err: countRes[0],
            msg: 'total查询失败'
          }
        }
      }).catch(err => {
        queRes['err'] = err;
        queRes['mgs'] = 'total查询失败'
      })

    }).catch(err => {
      console.log(err)
      queRes['err'] = err
    })
    ctx.body = queRes;
  }
});
router.post('/query_file_edit', async (ctx, next) => {
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    let token_userid = tokenRes.id;
    let queRes = {
      errCode: 1,
      msg: 'error'
    }
    let user_id = ctx.request.body.user_id;
    let file_id = ctx.request.body.file_id;

    if (tokenRes.id * 1 !== user_id * 1) {
      queRes['err'] = '用户登录状态出错，请重新登陆'
      ctx.body = queRes;
    }
    // console.log(user_id)
    let params = ['file_desc', 'file_id', 'file_score', 'file_title', 'user_id']
    // 查询数据库信息
    await mFile.query_file_edit(file_id, params, user_id).then(res => {
      console.log(res)
      queRes = {
        errCode: 0,
        msg: '数据获取成功',
        data: res[0]
      }
    }).catch(err => {
      console.log(err)
      queRes['err'] = err
    })
    ctx.body = queRes;
  }
});

router.post('/update_file_info', async (ctx, next) => {
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    let token_userid = tokenRes.id;
    let queRes = {
      errCode: 1,
      msg: 'error'
    }
    let INT_user_id = ctx.request.body.user_id;
    let INT_file_id = ctx.request.body.file_id;
    let file_title = ctx.request.body.file_title;
    let file_desc = ctx.request.body.file_desc;
    let INT_file_score = ctx.request.body.file_score;

    if (tokenRes.id * 1 !== INT_user_id * 1) {
      queRes['err'] = '用户登录状态出错，请重新登陆'
      ctx.body = queRes;
    } else {


      await mFile.update_file_info({
        file_title,
        file_desc,
        INT_file_score
      }, {
        INT_user_id,
        INT_file_id
      }).then(res => {
        // console.log(res)
        if (res.affectedRows === 1) {
          queRes = {
            errCode: 0,
            msg: '更新成功'
          }
        } else {
          queRes['err'] = res
        }
      }).catch(err => {
        queRes['err'] = err
      })

      ctx.body = queRes;
    }
  }
});


router.post('/delete_file', async (ctx, next) => {
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    let queRes = {
      errCode: 1,
      msg: 'error'
    }
    let user_id = ctx.request.body.user_id;
    let file_id = ctx.request.body.file_id;
    console.log(tokenRes)
    console.log(tokenRes.isAdmin + "" !== 'true')
    if (tokenRes.isAdmin + "" !== 'true' && tokenRes.id * 1 !== user_id * 1) {
      queRes['err'] = '用户登录状态出错，请重新登陆'
      ctx.body = queRes;
    } else {

      console.log(123)

      await mFile.delete_file(file_id).then(res => {
        console.log(res)
        if (res.affectedRows === 1) {
          queRes = {
            errCode: 0,
            msg: '删除成功'
          }
        } else {
          queRes['err'] = res
        }
      }).catch(err => {
        queRes['err'] = err
      })

      ctx.body = queRes;
    }
  }
});

// 获取文件资源的信息

router.post('/get_file_detail', async (ctx, next) => {

  let queRes = {
    errCode: 1,
    msg: 'error'
  }
  let auth_name = ctx.request.body.auth_name;
  let file_id = ctx.request.body.file_id;
  // console.log(user_id)

  // let params = ['file_desc','file_id','file_score','file_title','user_id']
  // 增加浏览量
  await mFile.add_file_bower(file_id).then(res => {}).catch(err => {
    console.log(err)
    queRes['err'] = err
  })
  console.log(queRes['err'])
  if (queRes['err']) {
    ctx.body = queRes;
    return;
  }
  // 查询数据库信息
  await mFile.get_file_detail(file_id).then(res => {
    // console.log(res)
    queRes = {
      errCode: 0,
      msg: '数据获取成功',
      data: res[0]
    }
  }).catch(err => {
    console.log(err)
    queRes['err'] = err
  })
  ctx.body = queRes;

});

// 点赞操作 start
router.post('/action_like_file', async (ctx) => {
  let file_id = ctx.request.body.file_id;
  let user_id = ctx.request.body.user_id;
  let auth_id = ctx.request.body.auth_id;
  let is_like = ctx.request.body.is_like;

  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    let likeRes = {
      errCode: 1,
      msg: 'error'
    };
    // console.log(is_like)
    if (is_like === 'true') {
      await mFile.create_like(user_id, file_id).then(async res => {
        if (res.affectedRows === 1) {
          await mFile.update_like(file_id, is_like).then(result => {
            if (result.affectedRows === 1) {
              likeRes = {
                errCode: 0,
                msg: 'success',
                action: 'add'
              };
            }
          })
        }
        // console.log(res)
      }).catch(err => {
        console.log(err)
      })

      let time = tool.format(new Date());
      // 检查bs_like_message是否已经存在该消息
      await mFile.query_message_file(file_id, user_id).then(async qRes => {
        if (qRes[0] && qRes[0]['message_id']) {
          // 更新bs_like_message
          await mFile.update_message_file(qRes[0]['message_id'], time).then(saveRes => {
            // console.log(saveRes)
            if (saveRes.affectedRows !== 1) {
              likeRes = {
                errCode: 1,
                msg: '更新点赞资源消息出错',
                err: saveRes,
              };
            }
          }).catch(err => {
            console.log(err)
            likeRes = {
              errCode: 1,
              msg: '更新点赞资源消息出错',
              err: err,
            };
          })
        } else {
          // 添加点赞消息
          await mFile.save_message_file(file_id, auth_id, user_id, time).then(saveRes => {
            // console.log(saveRes)
            if (saveRes.affectedRows !== 1) {
              likeRes = {
                errCode: 1,
                msg: '保存点赞资源消息出错',
                err: saveRes,
              };
            }
          }).catch(err => {
            console.log(err)
            likeRes = {
              errCode: 1,
              msg: '保存点赞资源消息出错',
              err: err,
            };
          })
        }
      }).catch(err => {
        console.log(err)
        likeRes = {
          errCode: 1,
          msg: 'query_message_file消息出错',
          err: err,
        };
      })










    } else {
      // console.log(33)
      await mFile.delete_like(user_id, file_id).then(async res => {
        if (res.affectedRows === 1) {
          await mFile.update_like(file_id, is_like).then(result => {
            if (result.affectedRows === 1) {
              likeRes = {
                errCode: 0,
                action: 'cancel',
                msg: 'success'
              };
            }
          })
        }
        // console.log(res)
      }).catch(err => {
        console.log(err)
      })
    }
    // 查询当前用户是否已经点赞
    await mFile.query_like(user_id, file_id).then(async res => {
      if (res[0]) {
        likeRes['islike'] = true;
      } else {
        likeRes['islike'] = false;
      }
    })
    // 查询当前文件的点赞数量
    await mFile.query_like_collect_amount(file_id).then(res => {

      if (res[0] && res[0].like_amount > -1) {
        likeRes['like_amount'] = res[0].like_amount;
      }
    }).catch(err => {
      console.log(err)
    })

    ctx.body = likeRes;
  }
})

// 获取文章是否已经点赞
router.post('/file_is_like', async (ctx) => {
  let file_id = ctx.request.body.file_id;
  let user_id = ctx.request.body.user_id;
  let queryRes = {
    errCode: 1,
    msg: 'error'
  };
  // 查询当前用户是否已经点赞
  await mFile.query_like(user_id, file_id).then(async res => {
    if (res[0]) {
      queryRes = {
        islike: true,
        errCode: 0,
        msg: 'success'
      };
    } else {
      queryRes = {
        islike: false,
        errCode: 0,
        msg: 'success'
      };
    }
  }).catch(err => {
    console.log(err);
    queryRes['err'] = err
  })
  ctx.body = queryRes
})
// 点赞操作 end

// 收藏操作 start
router.post('/action_collect_file', async (ctx) => {
  let file_id = ctx.request.body.file_id;
  let user_id = ctx.request.body.user_id;
  let is_collect = ctx.request.body.is_collect;
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    let collectRes = {
      errCode: 1,
      msg: 'error'
    };
    // console.log(is_collect)
    if (is_collect === 'true') {
      let time = tool.format(new Date())
      await mFile.create_collect(user_id, file_id, time).then(async res => {
        if (res.affectedRows === 1) {
          await mFile.update_collect(file_id, is_collect).then(result => {
            if (result.affectedRows === 1) {
              collectRes = {
                errCode: 0,
                msg: 'success',
                action: 'add'
              };
            }
          })
        }
        // console.log(res)
      }).catch(err => {
        console.log(err)
      })

    } else {
      await mFile.delete_collect(user_id, file_id).then(async res => {
        if (res.affectedRows === 1) {
          await mFile.update_collect(file_id, is_collect).then(result => {
            if (result.affectedRows === 1) {
              collectRes = {
                errCode: 0,
                action: 'cancel',
                msg: 'success'
              };
            }
          })
        }
        // console.log(res)
      }).catch(err => {
        console.log(err)
      })
    }
    // 查询当前用户是否已经收藏
    await mFile.query_collect(user_id, file_id).then(async res => {
      if (res[0]) {
        collectRes['iscollect'] = true;
      } else {
        collectRes['iscollect'] = false;
      }
    })
    // 查询当前文章的点赞数量
    await mFile.query_like_collect_amount(file_id).then(res => {
      // console.log(res)
      if (res[0] && res[0].collect_amount > -1) {
        collectRes['collect_amount'] = res[0].collect_amount;
      }
    }).catch(err => {
      console.log(err)
    })

    ctx.body = collectRes;
  }
})
// 获取文章是否已经点赞
router.post('/file_is_collect', async (ctx) => {
  let file_id = ctx.request.body.file_id;
  let user_id = ctx.request.body.user_id;
  let queryRes = {
    errCode: 1,
    msg: 'error'
  };
  // 查询当前用户是否已经收藏
  await mFile.query_collect(user_id, file_id).then(async res => {
    if (res[0]) {
      queryRes = {
        iscollect: true,
        errCode: 0,
        msg: 'success'
      };
    } else {
      queryRes = {
        iscollect: false,
        errCode: 0,
        msg: 'success'
      };
    }
  }).catch(err => {
    console.log(err);
    queryRes['err'] = err
  })
  ctx.body = queryRes
})
// 收藏操作 end

router.post('/download_file', async (ctx) => {
  let file_id = ctx.request.body.file_id;
  let user_id = ctx.request.body.user_id;
  let auth_id = ctx.request.body.auth_id;
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    let queryRes = {
      errCode: 1,
      msg: 'error'
    };
    let user_score = -1;
    let file_score = -1;
    let file_src = '';
    // 1、查询用户的积分
    await mUser.find_user({
      INT_id: user_id
    }, ['score']).then(res => {
      // console.log(res)
      if (res.length === 1) {
        user_score = res[0].score
      } else if (res.length === 0) {
        queryRes['msg'] = '当前用户查找失败，请重新登陆'
        queryRes['err'] = res
      } else {
        queryRes['err'] = res
      }
    }).catch(err => {
      queryRes['err'] = err
    });
    if (queryRes['err']) {
      ctx.body = queryRes
      return;
    }

    // 2、查询当前文件的所需积分
    await mFile.get_file_detail(file_id).then(res => {
      if (res.length === 1) {
        file_score = res[0].file_score
        file_src = res[0].file_src
      } else if (res.length === 0) {
        queryRes['msg'] = '当前文章查找失败，请刷新页面'
        queryRes['err'] = res
      } else {
        queryRes['err'] = res
      }
    }).catch(err => {
      queryRes['err'] = err
    });
    if (queryRes['err']) {
      ctx.body = queryRes
      return;
    }
    if (file_score > user_score) {
      queryRes = {
        errCode: 0,
        msg: '当前用户积分:' + user_score + '，文章所需积分:' + file_score + ',积分不足，请充值'
      }
      ctx.body = queryRes;
      return;
    }
    let isReduce = false
    // 3、进行积分扣除
    await mUser.update_score(user_id, file_score, 'reduce').then(res => {
      // console.log(res)
      if (res.affectedRows === 1) {
        // file_score = res[0].file_score
        isReduce = true
      } else if (res.length === 0) {
        queryRes['msg'] = '积分扣除失败，请刷新页面'
        queryRes['err'] = res
      } else {
        queryRes['err'] = res
      }
    }).catch(err => {
      queryRes['err'] = err
    });
    if (queryRes['err'] || !isReduce) {
      ctx.body = queryRes
      return;
    }
    //5、进行作者的积分返还
    await mUser.update_score(auth_id, file_score, 'add').then(res => {
      // console.log(res)
      if (res.affectedRows === 1) {
        // file_score = res[0].file_score
        isReduce = true
      } else if (res.length === 0) {
        queryRes['msg'] = 'auth积分添加失败，请刷新页面'
        queryRes['err'] = res
      } else {
        queryRes['err'] = res
      }
    }).catch(err => {
      queryRes['err'] = err
    });
    if (queryRes['err'] || !isReduce) {
      ctx.body = queryRes
      return;
    }

    // 5、创建文件下载任务信息
    let fileName = file_src.substring(file_src.lastIndexOf('/') + 1, file_src.length - 1)
    let createTask = false;
    let nowdate = new Date()
    let dateTime = tool.format(nowdate)
    let endTime = tool.format(new Date(nowdate.getTime() + downloadTimeRange))
    // 4.1|判断当前的下载任务是否存在过
    await mFile.query_download_task(user_id, file_id).then(async res => {
      // console.log(res);
      if (res[0] && res[0].task_id) {
        //下载任务存在
        await mFile.update_download_task_state(res[0].task_id, dateTime, endTime).then(res => {
          // console.log(res);

          if (res.affectedRows === 1) {
            // 任务创建成功
            queryRes = {
              errCode: 0,
              msg: '文件购买成功，五天内有效'
            }
            createTask = true
          } else {
            queryRes['msg'] = '下载任务状态修改失败'
            queryRes['err'] = res
          }
        }).catch(err => {
          console.log(err);
          queryRes['err'] = err
        });
      } else {
        // 4.2、下载任务没存在，则直接create新纪录
        await mFile.create_download_task(user_id, file_id, dateTime, endTime).then(res => {
          // console.log(res);

          if (res.affectedRows === 1) {
            // 任务创建成功
            createTask = true
            queryRes = {
              errCode: 0,
              msg: '文件购买成功，五天内有效'
            }
          } else {
            queryRes['msg'] = '下载任务状态创建失败'
            queryRes['err'] = res
          }
        }).catch(err => {
          console.log(err);
          queryRes['err'] = err
        });
      }
    }).catch(err => {
      console.log(err);
      queryRes['err'] = err
    });
    // 检测是否成功创建状态为0 的任务列表

    if (queryRes['err'] || !createTask) {
      // 5、回退问题？
      await mFile.query_download_task(user_id, file_id).then(async res => {
        if (res[0] && res[0].task_id) {
          // 任务数据成功，但是有错误，不回退？
        } else {
          // 回退积分
          await mUser.update_score(user_id, file_score, 'add').then(res => {
            // console.log(res)
            if (res.affectedRows === 1) {
              // file_score = res[0].file_score
              isReduce = true
            } else if (res.length === 0) {
              queryRes['msg'] = '下载任务发生错误，积分回退失败，请联系客服'
              queryRes['error'] = res
            } else {
              queryRes['error'] = res
            }
          }).catch(err => {
            queryRes['error'] = err
          });

          await mUser.update_score(auth_id, file_score, 'reduce').then(res => {
            // console.log(res)
            if (res.affectedRows === 1) {
              // file_score = res[0].file_score
              isReduce = true
            } else if (res.length === 0) {
              queryRes['msg'] = '下载任务发生错误，auth积分回退失败，请联系客服'
              queryRes['error'] = res
            } else {
              queryRes['error'] = res
            }
          }).catch(err => {
            queryRes['error'] = err
          });
        }
      })
    }



    // await mFile.download_file(user_id, file_id).then(async res => {

    // }).catch(err => {
    //   console.log(err);
    //   queryRes['err'] = err
    // })
    ctx.body = queryRes
  }
})
router.post('/check_is_download', async function (ctx) {
  let newDate = new Date()
  let file_id = ctx.request.body.file_id;
  let user_id = ctx.request.body.user_id;
  let queryRes = {
    errCode: 1,
    msg: 'error'
  }
  await mFile.query_download_task(user_id, file_id).then(async res => {
    console.log(res)
    if (res[0] && res[0].task_id) {
      queryRes = {
        errCode: 0,
        msg: '文件购买已过期，请重新下载',
        isEffective: false
      }

      // 检查有效期
      let dateTime = new Date(res[0].create_time)
      if (downloadTimeRange + dateTime.getTime() > newDate.getTime()) {
        queryRes = {
          errCode: 0,
          msg: '文件购买仍在有效期，请去下载中心下载文件',
          isEffective: true
        }

      }

    } else {
      if (res.length === 0) {
        queryRes = {
          errCode: 0,
          msg: '文件未购买过',
          isEffective: false
        }
      } else {
        queryRes['msg'] = '下载任务查询失败'
        queryRes['error'] = res
      }

    }
  }).catch(err => {
    queryRes['error'] = err
  });
  ctx.body = queryRes
})

// 获取已下载的列表信息
router.post('/get_download_list', async (ctx) => {
  let user_id = ctx.request.body.user_id;
  let pagesize = ctx.request.body.pagesize || 10
  let pageno = ctx.request.body.pageno || 0
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    if (tokenRes.id * 1 !== user_id * 1) {
      ctx.body = {
        errCode: 1,
        msg: '下载中心url有问题，当前登录用户和url用户不相同，请重新登录'
      };
      return;
    }
    let queryRes = {
      errCode: 1,
      msg: 'error'
    };
    await mFile.query_download_list(user_id, pagesize, pageno).then(async res => {
      console.log(res)
      let data = res;
      await mFile.query_download_list_total(user_id).then(countRes => {
        if (countRes[0] && countRes[0]['count(bs_download_list.file_id)'] > -1) {
          queryRes = {
            errCode: 0,
            data: data,
            total: countRes[0]['count(bs_download_list.file_id)'],
            msg: 'success'
          }
        } else {
          queryRes = {
            errCode: 1,
            err: countRes[0],
            msg: 'total查询失败'
          }
        }

      }).catch(err => {
        console.log(err)
        queryRes = {
          errCode: 1,
          err: err,
          msg: 'total查询失败'
        }
      })

    }).catch(err => {
      console.log(err)
      queryRes['msg'] = "获取下载列表出错"
      queryRes['err'] = err
    })
    ctx.body = queryRes

  }
})

//下载文件接口
router.get('/download', async function (ctx) {
  let file_id = ctx.query.file_id;

  let downloadRes = {
    errCode: 1,
    msg: 'error'
  }
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    // 查询文件路由
    await mFile.query_file_edit(file_id, ['file_src']).then(res => {
      console.log(res);
      if (res[0] && res[0].file_src) {
        file_src = res[0].file_src
      } else {
        downloadRes['err'] = res;
        downloadRes['msg'] = '资源id查找失败'
      }
    }).catch(err => {
      downloadRes['err'] = err
      console.log(err)
    })

    if (downloadRes['err']) {
      ctx.body = downloadRes;
      return;
    }
    let fileName = file_src.substring(file_src.lastIndexOf('/') + 1)
    let fileDir = file_src.substring(0, file_src.lastIndexOf('/'))

    // // Set Content-Disposition to "attachment" to signal the client to prompt for download.
    // // Optionally specify the filename of the download.
    // // 设置实体头（表示消息体的附加信息的头字段）,提示浏览器以文件下载的方式打开
    // // 也可以直接设置 ctx.set("Content-disposition", "attachment; filename=" + fileName);
    // // ctx.attachment(fileName);
    // // await send(ctx, fileName, { root: './private/files//4/' });

    // // 设置响应头通知通知浏览器以二进制文件接收
    // ctx.set('Content-Type', 'application/octet-stream')

    // let fileName = '1616597131815工作簿1.xlsx'
    // let fileDir = './private/files/4'
    const dir = path.join(fileDir) // 静态资源目录
    ctx.attachment(fileName)
    console.log(dir)
    console.log(fileName)
    try {
      await send(ctx, fileName, {
        root: dir
      })
    } catch (e) {
      console.log(e)
      ctx.throw(404, '文件不存在')
    }
  }
});
// 获取index 的资源
router.post('/get_file_sort', async function (ctx) {
  let sortStr = ctx.request.body.sort;
  let pagesize = ctx.request.body.pagesize || 10
  let pageno = ctx.request.body.pageno || 0
  let params = ['file_desc', 'bower_amount', 'collect_amount', 'file_id', 'file_score', 'file_title', 'like_amount', 'upload_time', 'user_id']
  let quertRes = {
    errCode: 1,
    msg: 'error'
  }
  await mFile.get_file_sort(sortStr, params, pageno, pagesize).then(async res => {
    let data = res
    await mFile.get_file_total().then(result => {
      console.log(result)
      if (result[0] && result[0]['count(*)'] > -1) {
        quertRes = {
          errCode: 0,
          msg: 'success',
          data: data,
          total: result[0]['count(*)']
        }
      } else {
        quertRes['err'] = result
        quertRes['msg'] = '获取资源总数失败'
      }
    })

  }).catch(err => {
    quertRes['err'] = err
    quertRes['msg'] = '获取资源信息失败'
  })

  ctx.body = quertRes
})
// 获取收藏的文件列表
router.post('/query_collect_file', async (ctx) => {
  let user_id = ctx.request.body.user_id;
  let pagesize = ctx.request.body.pagesize || 10
  let pageno = ctx.request.body.pageno || 0
  let tokenRes = tool.checkToken(ctx);
  if (tokenRes.code === 1) {
    ctx.body = tokenRes.body;
  } else {
    let queryRes = {
      errCode: 1,
      msg: 'error'
    }
    await mFile.query_collect_file(user_id, pageno, pagesize).then(async res => {
      // console.log(res)
      let data = res;
      await mFile.query_collect_file_total(user_id).then(countRes => {
        // console.log(countRes)
        if (countRes[0] && countRes[0]['count(*)'] > -1) {
          queryRes = {
            errCode: 0,
            data: data,
            total: countRes[0]['count(*)'],
            msg: 'success'
          }
        } else {
          queryRes = {
            errCode: 1,
            err: countRes[0],
            msg: 'total查询失败'
          }
        }

      }).catch(err => {
        console.log(err)
        queryRes = {
          errCode: 1,
          err: err,
          msg: 'total查询失败'
        }
      })
    }).catch(err => {
      console.log(err)
      queryRes['msg'] = '文章获取失败';
      queryRes['err'] = err;

    })
    ctx.body = queryRes
  }
})
module.exports = router.routes();