/*
 * @Author: yenible
 * @Date: 2021-04-11 15:00:31
 * @LastEditors: yenible
 * @LastEditTime: 2021-04-14 14:10:06
 * @Description: xxx
 */
const mChat = require('../models/model_chat')
const tool = require('../lib/util/tool');

const chatSocket = {
  send_message: async function (data, io) {
    console.log(data)
    let nowtime = tool.format(new Date())
    let receive_userid = -1;
    let errMesg;
    // 获取receive的id
    // await mChat.query_user_id(data.receive_usenamer).then(res=>{
    //   console.log(res)
    //   if(res[0] && res[0].id*1>-1){
    //     receive_userid = res[0].id
    //   }else{
    //     errMesg = res
    //   }
    // }).catch(err=>{
    //   errMesg = err
    // })
    // if(errMesg ||receive_userid===-1 ){
    //   io.emit('error', {
    //     'err': errMesg,
    //     'msg': '获取接受对象的id出错'
    //   })
    //   return;
    // }
    // 处理聊天数据，存入数据库？提醒双方更新，若存在？

    let room_id = -1;
    // 检查该聊天室是否有建立
    await mChat.query_chat_room(data.send_user, data.receive_user).then(async res => {
      console.log(res)
      if (res && res[0]) {
        room_id = res[0].room_id
      } else {
        // 创建聊天室
        await mChat.create_chat_room(data.send_user, data.receive_user,nowtime).then(createRes => {
          console.log(createRes)
          if (createRes.affectedRows * 1 === 1) {
            room_id = createRes.insertId
          } else {
            errMesg = createRes
          }
        }).catch(err => {
          console.log(err)
          errMesg = err
        })
      }

    }).catch(err => {
      console.log(err)
      errMesg = err
    })
    if (errMesg || room_id === -1) {
      io.emit('error', {
        'err': errMesg,
        'msg': '保存聊天室id出错'
      })
      return;
    }
    console.log("roomid"+room_id)
    await mChat.add_chat_message(data.send_user, data.receive_user, data.message, room_id, nowtime).then(res => {
      // console.log(res)
      if (res.affectedRows * 1 !== 1) {
        errMesg = res
      }
    }).catch(err => {
      console.log(err)
      errMesg = err
    })
    if (errMesg) {
      io.emit('error', {
        'err': errMesg,
        'msg': '保存聊天消息出错'
      })
      return;
    }
    //     io.emit('refresh', {
    //       chat_id:3,
    // receive_userid:26,
    // remark:0,
    // send_content:"111",
    // send_time:"2021-04-11T08:52:09.000Z",
    // send_userid:4

    //     })
    io.emit('refresh',{send_user:data.send_user,receive_user: data.receive_user,send_time:nowtime,send_content:data.message,room_id:room_id})
    // 更新聊天列表
    io.emit('refreshChatList')
  }
}
module.exports = chatSocket