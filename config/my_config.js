/*
 * @Author: yenible
 * @Date: 2020-12-12 15:42:49
 * @LastEditors: yenible
 * @LastEditTime: 2021-04-09 20:17:56
 * @Description: xxx
 */
// const config = {
//     port: 33333,
//     database:{database: 'yenible_bs',
//     user: 'ye',
//     password: '123123',
//     host: '106.14.37.152'}
// }
const config = {
    port: 33333,
    database: {
        database: 'yenible_bs',
        user: 'root',
        password: '123456',
        host: 'localhost'
    },
    blogImgURL: 'http://localhost:33333/uploads/article', // 静态资源服务器域名地址，映射关系  ibolg.img/ F:/code/vscode/bs/bsCode/public/upload/images
    avatarImgUrl:'http://localhost:33333/uploads/avatar',
    imgLocalSrc: './public/uploads/images', //图片缓存相对地址
    articleImgSrc: './public/uploads/article',//文章图片存放的相对地址
    avatarImgSrc: './public/uploads/avatar',//头像图片存放相对地址
    fileSrc: './private/files/',//文件资源存放相对地址
    downloadTimeRange:5*24*60*60*1000,//五天有效
    socketPort:33335,//用于socket的端口设置
}
module.exports = config