/*
 * @Author: yenible
 * @Date: 2021-03-28 19:37:30
 * @LastEditors: yenible
 * @LastEditTime: 2021-04-05 20:38:34
 * @Description: xxx
 */

// 引用mysql文件连接数据库
const mysql = require('../lib/mysql_connect');
// const gravatar = require('gravatar');
const tool = require('../lib/util/tool')

class search {
  constructor() {}

  search_article_myblogs(myblogsType, user_id, searchText, searDate, pagesize, pageno) {
    let sqlStr = `
    select id,article_id,article_content,update_time,article_title,publish_type,article_type,article_state,like_amount,bower_amount,article_collect_amount,bs_user.name  
    from bs_article join bs_user 
    on bs_article.user_id =bs_user.id
    where 
    user_id = ` + user_id + ` 
     and article_state="` + myblogsType + `"  and 
    article_title  like '%` + searchText + `%'  and
    update_time like '%` + searDate + `%' 
    order by  update_time  DESC  limit 
    ` + pageno * pagesize + ', ' + pagesize

    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  search_article_myblogs_total(myblogsType, user_id, searchText, searDate) {
    let sqlStr = `
    select count(article_id)
    from bs_article join bs_user 
    on bs_article.user_id =bs_user.id
    where 
    user_id = ` + user_id + ` 
     and article_state="` + myblogsType + `"  and 
    article_title  like '%` + searchText + `%' and
    update_time like '%` + searDate + `%' 

    `

    console.log(sqlStr)
    return mysql.query(sqlStr);
  }

  search_file_myblogs(user_id, searchText, searDate, pagesize, pageno) {
    let sqlStr = `
    select file_desc,bower_amount,collect_amount,file_id,file_score,file_title,like_amount,upload_time,user_id,name 
    from bs_file inner join  bs_user on user_id = id 
    where 
    user_id = ` + user_id + ` and 
    file_title  like '%` + searchText + `%'  and
    upload_time like '%` + searDate + `%' 
    order by  upload_time  DESC  limit 
    ` + pageno * pagesize + ', ' + pagesize

    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  search_file_myblogs_total(user_id, searchText, searDate) {
    let sqlStr = `
    select count(*)
    from bs_file inner join  bs_user on user_id = id 
    where  
    user_id = ` + user_id + `  and 
    file_title  like '%` + searchText + `%'  and
    upload_time like '%` + searDate + `%' 

    `

    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  search_user_name(searchText) {
    let sqlStr = `
    select avatar,id,name 
    from bs_user 
	  where name like '%` + searchText + `%'
    `

    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  search_blog_global(searchText, pagesize, pageno) {
    let sqlStr = `
    select name, bs_article.article_id,article_content,article_title,user_id,article_type,like_amount,bower_amount,article_collect_amount,article_state,update_time,publish_type
    from bs_article inner join bs_user on id = user_id
    where
    publish_type  = 1  and 
    (article_title like  '%` + searchText + `%' or 
 article_content like  '%` + searchText + `%'  )
 limit  
 ` + pageno * pagesize + ', ' + pagesize
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  search_blog_global_total(searchText) {
    let sqlStr = `
select count(*)
    from bs_article
    where
    publish_type  = 1  and 
    (article_title like  '%` + searchText + `%' or 
 article_content like  '%` + searchText + `%'  )`
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }

  // 文件查询
  search_file_global(searchText, pagesize, pageno) {
    let sqlStr = `
    select file_desc,bower_amount,collect_amount,file_id,file_score,file_title,like_amount,upload_time,user_id,name 
    from bs_file inner join  bs_user on user_id = id 
    where 
    file_title  like '%` + searchText + `%'  or
    file_desc  like '%` + searchText + `%'  
    limit 
    ` + pageno * pagesize + ', ' + pagesize

    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  search_file_global_total(searchText) {
    let sqlStr = `
select count(*)
from bs_file inner join  bs_user on user_id = id 
where 
file_title  like '%` + searchText + `%'  or
file_desc  like '%` + searchText + `%'  
`
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
}
// console.log(new user("ye", 123, "944329384@qq.com", 555).register())
module.exports = new search()