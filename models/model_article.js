/*
 * @Author: yenible
 * @Date: 2021-01-12 17:27:09
 * @LastEditors: yenible
 * @LastEditTime: 2021-04-22 20:43:00
 * @Description: xxx
 */
// 引用mysql文件连接数据库
const mysql = require('../lib/mysql_connect');

// 当前模型主要操作的表
const table = 'bs_article';
const imgTable = 'bs_img_article'

class article {
  constructor() {}
  add_article(dataObj) {
    // let sqlStr = "insert into "+ table + " (" + Object.keys(dataObj).join(", ") + ", create_time, login_time) value ('" + Object.values(dataObj).join("', '") +"') "
    let sqlStr = "insert into " + table + "  "
    let preStr = " (";
    let nextStr = ") value (";
    Object.keys(dataObj).forEach((item, index) => {
      if (dataObj[item]) {
        // 检测当前的字段类型是否是INT
        if (item.substring(0, 4) === 'INT_') {
          preStr += item.substring(4) + ",";
          nextStr += dataObj[item] + ","
        } else {
          preStr += item + ",";
          nextStr += "\"" + dataObj[item] + "\",";
        }

      }
    })
    sqlStr += preStr.substring(0, preStr.length - 1) + nextStr.substring(0, nextStr.length - 1) + ")";

    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 更新bs_artcle的article_amount
  update_article_amount(user_id,action){
    let sqlStr;
    if(action==='add'){
      sqlStr = "update bs_user set article_amount=article_amount+1 where id = " + user_id 
    }else if('remove'){
      sqlStr = "update bs_user set article_amount=article_amount-1 where id = " + user_id 
    }
    console.log(sqlStr)
    return mysql.query(sqlStr);    
  }
  // 更新文章信息
  update_article(dataObj, id) {
    let sqlStr = 'update ' + table + ' set '
    Object.keys(dataObj).forEach((item, index) => {
      if (dataObj[item]) {
        // 检测当前的字段类型是否是INT
        if (item.substring(0, 4) === 'INT_') {
          sqlStr += item.substring(4) + "=" + dataObj[item] + ", ";
        } else {
          sqlStr += item + "=" + "\"" + dataObj[item] + "\", ";
        }
      }
    });
    // 逗号后面还有个空格所以是-2
    sqlStr = sqlStr.substring(0, sqlStr.length - 2) + " where article_id = '" + id + "'"
    // console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 查询当前用户写过的文章
  query_user_article(userid, conditionObj, params, pagesize, pageno, sort) {
    let sqlStr = 'select ';
    params.forEach(item => {
      sqlStr += item + ",";
    });
    sqlStr += 'bs_user.name';
    // sqlStr += ' from bs_article order by update_time where user_id = ' + userid + ' limit ' + pageno*pagesize + ', ' + pagesize + '  '
    sqlStr += '  from bs_article,bs_user where bs_user.id=bs_article.user_id and user_id = ' + userid
    if (conditionObj) {
      Object.keys(conditionObj).forEach(item => {
        if (conditionObj[item]) {
          // 检测当前的字段类型是否是INT
          if (item.substring(0, 4) === 'INT_') {
            sqlStr += ' and ' + item.substring(4) + "=" + conditionObj[item] + " ";
          } else {
            sqlStr += ' and ' + item + "=" + "\"" + conditionObj[item] + "\"  ";
          }
        }
      })
    }
    sqlStr += ' order by '+ sort +' DESC  ' + ' limit ' + pageno * pagesize + ', ' + pagesize
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }

  // 查询当前用户写过的文章通过用户名查询
  query_username_article(username, conditionObj, params, pagesize, pageno, sort) {
    let sqlStr = 'select ';
    params.forEach(item => {
      sqlStr += item + ",";
    });
    sqlStr += 'bs_user.name';
    sqlStr += "  from bs_article,bs_user where bs_user.id=bs_article.user_id and name = '" + username +"' "
    if (conditionObj) {
      Object.keys(conditionObj).forEach(item => {
        if (conditionObj[item]) {
          // 检测当前的字段类型是否是INT
          if (item.substring(0, 4) === 'INT_') {
            sqlStr += ' and ' + item.substring(4) + "=" + conditionObj[item] + " ";
          } else {
            sqlStr += ' and ' + item + "=" + "\"" + conditionObj[item] + "\"  ";
          }
        }
      })
    }
    sqlStr += ' order by '+ sort +' DESC  ' + ' limit ' + pageno * pagesize + ', ' + pagesize
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  query_user_article_total(userid, conditionObj) {
    let sqlStr = 'select count(*)';
    if(userid){
      sqlStr += '  from bs_article where user_id = ' + userid
    if (conditionObj) {
      Object.keys(conditionObj).forEach(item => {
        if (conditionObj[item]) {
          // 检测当前的字段类型是否是INT
          if (item.substring(0, 4) === 'INT_') {
            sqlStr += ' and ' + item.substring(4) + "=" + conditionObj[item] + " ";
          } else {
            sqlStr += ' and ' + item + "=" + "\"" + conditionObj[item] + "\"  ";
          }
        }
      })
    }
    }else{
      sqlStr += '  from bs_article ' 

    }
    
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 通过name查询文章总数
  query_username_article_total(username, conditionObj) {
    let sqlStr = "select count(bs_article.article_id) from  bs_article ,bs_user  where  bs_user.name = '" + username + "'  and bs_article.user_id  = bs_user.id "
    if (conditionObj) {
      Object.keys(conditionObj).forEach(item => {
        if (conditionObj[item]) {
          // 检测当前的字段类型是否是INT
          if (item.substring(0, 4) === 'INT_') {
            sqlStr += ' and ' + item.substring(4) + "=" + conditionObj[item] + " ";
          } else {
            sqlStr += ' and ' + item + "=" + "\"" + conditionObj[item] + "\"  ";
          }
        }
      })
    }
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 查询文章id是否已经存在,第二个参数为需要获取的字段
  query_article(id, params) {
    // 检测当前的文章id是否已存在
    // let sqlStr = 'select article_id from ' + table + " where article_id = '" + id + "'";
    let sqlStr = 'select ';
    params.forEach(item => {
      sqlStr += item + ",";
    });
    sqlStr = sqlStr.substring(0, sqlStr.length - 1);
    sqlStr += " from " + table + " where article_id = '" + id + "'";
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  delete_article(article_id) {
    let sqlStr = "delete from bs_article where article_id = '" + article_id + "'  ";
    console.log(sqlStr)
    return mysql.query(sqlStr)
  }
  // 查询指定文章的全部信息
  get_article_detail(article_id) {
    console.log(article_id)
    let sqlStr = `select article_id,update_time,article_title,publish_type,fans_amount,article_type,article_state,like_amount,bower_amount,article_collect_amount,bs_user.name,article_content,article_amount,focus_amount,user_collect_amount,score,id  from bs_article,bs_user where id = user_id and article_id = '` + article_id + `' `
    console.log(sqlStr);
    return mysql.query(sqlStr)
  }
  // 增加浏览量
  add_article_bower(article_id){
    let sqlStr = "update bs_article set bower_amount=bower_amount+1 where article_id = '" + article_id + "'";
    console.log(sqlStr);
    return mysql.query(sqlStr)
  }
  // 文章添加图片
  add_img(dataObj) {
    let sqlStr = "insert into " + imgTable + " (img_name, img_src, article_id) value (";
    sqlStr += "'" + dataObj.img_name + "', " + "'" + dataObj.img_src + "', " + "'" + dataObj.article_id + "')";
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  save_label(label_id, article_id) {
    let sqlStr = "insert into bs_article_label (label_id,article_id) value(" + label_id + ", '" + article_id + "')"
    console.log(sqlStr)
    return mysql.query(sqlStr)
  }
  remove_label(label_id, article_id) {
    let sqlStr = "delete from bs_article_label where article_id = '" + article_id + "' and label_id = " + label_id
    console.log(sqlStr)
    return mysql.query(sqlStr)
  }
  query_article_label(article_id) {
    let sqlStr = "select label_id,article_id from bs_article_label where  article_id = '" + article_id + "'"
    console.log(sqlStr)
    return mysql.query(sqlStr)
  }
  // 获取文章标签信息
  get_label() {
    // 内连接
    let sqlStr = "select label_id,label_name,label_type,type_name from bs_label_list  inner join bs_article_type on bs_label_list.label_type = bs_article_type.type_id"
    console.log(sqlStr)
    return mysql.query(sqlStr)
  }
  // 获取文章类型列表
  get_article_type_list(){
    let sqlStr = "select type_id, type_name from bs_article_type"
    console.log(sqlStr)
    return mysql.query(sqlStr)
  }
  // 点赞的接口操作
  // 1、创建bs_user_article_like的关联数据
  create_like(user_id, article_id) {
    let sqlStr = 'insert into bs_user_article_like (user_id,article_id) value (' + user_id + ", '" + article_id + "')"
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 2、删除bs_user_article_like的数据，取消点赞
  delete_like(user_id, article_id) {
    let sqlStr = 'delete from bs_user_article_like where user_id =' + user_id + " and article_id = '" + article_id + "'"
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 3、更新文章的点赞数量
  update_like(article_id, is_like) {
    let sqlStr = '';
    if (is_like === 'true') {
      sqlStr = "update bs_article set like_amount=like_amount+1 where article_id = '" + article_id + "'"
    } else {
      sqlStr = "update bs_article set like_amount=like_amount-1 where article_id = '" + article_id + "'"
    }
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 查询当前文章该用户是否有点赞
  query_like(user_id, article_id) {
    let sqlStr = "select user_id,article_id  from bs_user_article_like where article_id = '" + article_id + "' and user_id =" + user_id
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 查询点赞和收藏的数量
  query_like_collect_amount(article_id) {
    let sqlStr = "select article_id,like_amount,article_collect_amount  from bs_article where article_id = '" + article_id + "' "
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }

  // 收藏接口操作
  // 1、创建收藏的关联数据
  create_collect(user_id, article_id,time) {
    let sqlStr = 'insert into bs_user_article_collect (user_id,article_id,create_time) value (' + user_id + ", '" + article_id + "','"+time+"')"
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 2、删除bs_user_article_collect的数据，取消收藏
  delete_collect(user_id, article_id) {
    let sqlStr = 'delete from bs_user_article_collect where user_id =' + user_id + " and article_id = '" + article_id + "'"
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 3、更新文章的点赞数量
  update_collect(article_id, is_like) {
    let sqlStr = '';
    if (is_like === 'true') {
      sqlStr = "update bs_article set article_collect_amount=article_collect_amount+1 where article_id = '" + article_id + "'"
    } else {
      sqlStr = "update bs_article set article_collect_amount=article_collect_amount-1 where article_id = '" + article_id + "'"
    }
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 查询当前文章该用户是否有收藏
  query_collect(user_id, article_id) {
    let sqlStr = "select user_id,article_id  from bs_user_article_collect where article_id = '" + article_id + "' and user_id =" + user_id
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 关注接口操作
  // 1、创建/删除关注表的记录
  change_attract(user_id, auth_id, is_collect) {
    let sqlStr;
    if (is_collect === 'true') {
      sqlStr = 'insert into bs_user_attract (user_id, auth_id) value (' + user_id + ", " + auth_id + ")"
    } else {
      sqlStr = 'delete from bs_user_attract where user_id =' + user_id + " and auth_id = " + auth_id
    }
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 2、更改user的关注数量
  update_focus_amount(user_id, is_collect) {
    let sqlStr = '';
    if (is_collect === 'true') {
      sqlStr = "update bs_user set focus_amount=focus_amount+1 where id = " + user_id
    } else {
      sqlStr = "update bs_user set focus_amount=focus_amount-1 where id = " + user_id
    }
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 3、修改auth的粉丝数量
  // 2、更改user的关注数量
  update_fans_amount(auth_id, is_collect) {
    let sqlStr = '';
    if (is_collect === 'true') {
      sqlStr = "update bs_user set fans_amount=fans_amount+1 where id = " + auth_id
    } else {
      sqlStr = "update bs_user set fans_amount=fans_amount-1 where id = " + auth_id
    }
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 查询是否关注该auth
  query_attract(user_id, auth_id) {
    let sqlStr = "select user_id, auth_id  from bs_user_attract where auth_id = " + auth_id + " and user_id =" + user_id
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }

  // 获取用户关注列表
  query_attract_list(userid) {
    let sqlStr = 'select avatar,user_id,auth_id,name from bs_user_attract,bs_user where bs_user.id=bs_user_attract.auth_id and user_id=' + userid;
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 获取用户粉丝列表
  // 获取用户关注列表
  query_fans_list(authid) {
    let sqlStr = 'select avatar,user_id,auth_id,name from bs_user_attract,bs_user where bs_user.id=bs_user_attract.user_id and auth_id=' + authid;
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  delete_attract(auth_id,user_id){
    let sqlStr = "delete from bs_user_attract where auth_id = " + auth_id + " and user_id = " + user_id;
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  get_article_sort(article_type,sortStr,pageno,pagesize){
    let sqlStr = `
    select  name,bs_article.article_id,article_content,article_title,user_id,article_type,like_amount,bower_amount,article_collect_amount,article_state,update_time,publish_type
    from (((bs_article inner join bs_article_label on bs_article.article_id  = bs_article_label.article_id ) inner join bs_label_list  on  bs_article_label.label_id = bs_label_list.label_id)
inner join bs_article_type  on bs_label_list.label_type = bs_article_type.type_id) inner join bs_user on bs_user.id = bs_article.user_id
    where 
    bs_article.publish_type = 1 and 
    bs_article_type.type_id = `+article_type
    if(sortStr==='new'){
      sqlStr += ` order by update_time DESC`
    }else {
      sqlStr += ` order by like_amount `
    }
    sqlStr += ' limit ' + pageno * pagesize + ', ' + pagesize
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  get_article_sort_total(article_type){
    let sqlStr = `
    select   count(bs_article.article_id)
    from (((bs_article inner join bs_article_label on bs_article.article_id  = bs_article_label.article_id ) inner join bs_label_list  on  bs_article_label.label_id = bs_label_list.label_id)
inner join bs_article_type  on bs_label_list.label_type = bs_article_type.type_id) inner join bs_user on bs_user.id = bs_article.user_id
    where 
    bs_article.publish_type = 1 and 
    bs_article_type.type_id = `+article_type
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 查询收藏文件
  query_collect_article(user_id,pageno,pagesize){
    let sqlStr = `
    select  name,bs_user_article_collect.create_time,bs_article.article_id,article_content,article_title,bs_article.user_id,article_type,like_amount,bower_amount,article_collect_amount,article_state,update_time,publish_type
    from (bs_user_article_collect inner  join bs_article on bs_article.article_id = bs_user_article_collect.article_id)
inner  join bs_user on bs_user.id = bs_user_article_collect.user_id 

    where bs_user_article_collect.user_id = `+user_id+`
    order by bs_user_article_collect.create_time DESC limit 
    ` + pageno * pagesize + ', ' + pagesize
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  query_collect_article_total(user_id){
    let sqlStr = `
    select count(*)
    from (bs_user_article_collect inner  join bs_article on bs_article.article_id = bs_user_article_collect.article_id)
    where bs_user_article_collect.user_id = `+user_id
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 保存点赞消息
  save_message_article(article_id,auth_id,user_id,time){
    let sqlStr = 'insert into bs_like_message (user_id,article_id,reply_id,reply_time,reply_type) value (' + auth_id + ", '" + article_id + "',"+user_id+",'"+time+"','article')"
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 查询点赞消息信息
  query_message_article(article_id,user_id){
    let sqlStr =  "select message_id from bs_like_message where article_id='"+article_id+"' and reply_id = "+user_id
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  update_message_article(message_id,time){
    let sqlStr = "update bs_like_message set reply_time='"+time+"' , is_see = 0 where message_id="+message_id
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 保存关注信息
  save_message_attract(user_id, auth_id,time){
    let sqlStr = 'insert into bs_attract_message (user_id,reply_id,reply_time) value (' + auth_id + ","+user_id+",'"+time+"')"
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 查询关注消息信息
  query_message_attract(user_id, auth_id){
    let sqlStr =  "select message_id from bs_attract_message where user_id="+auth_id+" and reply_id = "+user_id
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  update_message_attract(message_id,time){
    let sqlStr = "update bs_attract_message set reply_time='"+time+"' , is_see = 0 where message_id="+message_id
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
   
  create_label(label_name,type_id){

    let sqlStr = "insert into bs_label_list (label_type,label_name) value ("+type_id+",'"+label_name+"')"
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
}
module.exports = new article()