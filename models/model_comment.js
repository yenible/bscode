/*
 * @Author: yenible
 * @Date: 2021-03-12 20:53:36
 * @LastEditors: yenible
 * @LastEditTime: 2021-04-17 22:42:34
 * @Description: xxx
 */

// 引用mysql文件连接数据库
const mysql = require('../lib/mysql_connect');
// const gravatar = require('gravatar');
const tool = require('../lib/util/tool')

class comment {
  constructor() {}
  // 保存文章评论信息
  save_article_comment(dataObj) {
    // 拼接sql语句
    let sqlStr;
    let nextsqlStr = "";
    let presqlStr = "";
    Object.keys(dataObj).forEach(item => {
      console.log(item)
      if (item.substring(0, 4) === 'INT_') {
        presqlStr += item.substring(4, item.length) + ",";
        nextsqlStr += dataObj[item] + ",";
      } else {
        presqlStr += item + ",";
        nextsqlStr += "'" + dataObj[item] + "',";
      }
    });
    sqlStr = "insert into bs_article_comment (" + presqlStr.substring(0, presqlStr.length - 1) + ") value (" + nextsqlStr.substring(0, nextsqlStr.length - 1) + ")"


    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 查询文章的全部评论数据
  query_article_all_comment(article_id) {
    let sqlStr = "select comment_id,comment_content, bs_article_comment.create_time,depth,thread,parent_id,user_id,name,avatar from bs_article_comment,bs_user where bs_user.id=bs_article_comment.user_id and article_id = '" + article_id + "'"
    console.log(sqlStr)
    return mysql.query(sqlStr);
  };
  // 保存文件评论信息
  save_file_comment(dataObj) {
    // 拼接sql语句
    let sqlStr;
    let nextsqlStr = "";
    let presqlStr = "";
    Object.keys(dataObj).forEach(item => {
      console.log(item)
      if (item.substring(0, 4) === 'INT_') {
        presqlStr += item.substring(4, item.length) + ",";
        nextsqlStr += dataObj[item] + ",";
      } else {
        presqlStr += item + ",";
        nextsqlStr += "'" + dataObj[item] + "',";
      }
    });
    sqlStr = "insert into bs_file_comment (" + presqlStr.substring(0, presqlStr.length - 1) + ") value (" + nextsqlStr.substring(0, nextsqlStr.length - 1) + ")"


    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 查询文章的全部评论数据
  query_file_all_comment(file_id) {
    let sqlStr = "select comment_id,comment_content, bs_file_comment.create_time,depth,thread,parent_id,user_id,name,avatar from bs_file_comment,bs_user where bs_user.id=bs_file_comment.user_id and file_id = " + file_id
    console.log(sqlStr)
    return mysql.query(sqlStr);
  };
  delete_sons_comment(comment_id){
    let sqlStr =" delete from bs_file_comment WHERE thread LIKE '%/"+comment_id+"/%'"
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  delete_sons_comment_article(comment_id){
    let sqlStr =" delete from bs_article_comment WHERE thread LIKE '%/"+comment_id+"/%'"
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  delete_comment(comment_id){
    let sqlStr =" delete from bs_file_comment WHERE comment_id = " + comment_id
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  delete_comment_article(comment_id){
    let sqlStr =" delete from bs_article_comment WHERE comment_id = " + comment_id
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  query_article_auth(article_id){
    let sqlStr = 
      "select user_id from bs_article where article_id = '"+article_id+"'"
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 保存message
  save_message_article(dataObj){
    let sqlStr;
    let nextsqlStr = "";
    let presqlStr = "";
    Object.keys(dataObj).forEach(item => {
      console.log(item)
      if (item.substring(0, 4) === 'INT_') {
        presqlStr += item.substring(4, item.length) + ",";
        nextsqlStr += dataObj[item] + ",";
      } else {
        presqlStr += item + ",";
        nextsqlStr += "'" + dataObj[item] + "',";
      }
    });
    sqlStr = "insert into bs_article_file_message (" + presqlStr.substring(0, presqlStr.length - 1) + ") value (" + nextsqlStr.substring(0, nextsqlStr.length - 1) + ")"


    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 查找parent_id的user
  query_article_parent_comment(id){
    let sqlStr = 
      "select user_id from bs_article_comment where comment_id = " + id
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 保存file的message
  query_file_auth(file_id){
    let sqlStr = 
      "select user_id from bs_file where file_id = '"+file_id+"'"
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 保存message
  save_message_file(dataObj){
    let sqlStr;
    let nextsqlStr = "";
    let presqlStr = "";
    Object.keys(dataObj).forEach(item => {
      console.log(item)
      if (item.substring(0, 4) === 'INT_') {
        presqlStr += item.substring(4, item.length) + ",";
        nextsqlStr += dataObj[item] + ",";
      } else {
        presqlStr += item + ",";
        nextsqlStr += "'" + dataObj[item] + "',";
      }
    });
    sqlStr = "insert into bs_article_file_message (" + presqlStr.substring(0, presqlStr.length - 1) + ") value (" + nextsqlStr.substring(0, nextsqlStr.length - 1) + ")"


    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 查找parent_id的user
  query_file_parent_comment(id){
    let sqlStr = 
      "select user_id from bs_file_comment where comment_id = " + id
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
}
// console.log(new user("ye", 123, "944329384@qq.com", 555).register())
module.exports = new comment()