/*
 * @Author: yenible
 * @Date: 2021-04-11 15:53:25
 * @LastEditors: yenible
 * @LastEditTime: 2021-04-12 23:42:11
 * @Description: xxx
 */

// 引用mysql文件连接数据库
const mysql = require('../lib/mysql_connect');
// const gravatar = require('gravatar');
const tool = require('../lib/util/tool')

class chat {
  constructor() {}
  // 通过用户名获取用户id
  query_user_id(user_name){
    let sqlStr = `
      select id from bs_user where name =  '`+user_name+"'";
      console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  add_chat_message(send_userid,receive_userid,send_content,room_id,nowtime) {
    let sqlStr = `
    insert into bs_chat (room_id,send_userid,receive_userid,send_content,send_time) 
    value(`+room_id+","+send_userid+","+receive_userid+",'"+send_content+"','"+nowtime+"')"
     
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  
  get_chat_message(user_id_1,user_id_2){
    let sqlStr = `
    select bs_chat.* 
    from bs_chat inner join bs_chat_room on bs_chat.room_id =bs_chat_room.room_id 
    where (send_userid =${user_id_1} and receive_userid =${user_id_2} and  send_userid_delete=0 ) or
     (send_userid =${user_id_2} and receive_userid =${user_id_1} and receive_userid_delete=0)
    `
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  get_recipient_info(username){
    let sqlStr = `
      select name,id,avatar 
      from bs_user 
      where name = '${username}'
    `
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 查询最新的数据
  get_latest_chat_list(userid){
    let sqlStr = `
    select a.*  ,name as send_user_name,avatar as send_user_avatar
    from bs_chat a inner join 
    (select max(send_time) as send_time from bs_chat group by room_id) b on a.send_time = b.send_time 
    inner join bs_user on bs_user.id = a.send_userid 
    where a.receive_userid = ${userid} or  a.send_userid = ${userid}
    group by chat_id order by a.send_time DESC
    ` 
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 查询最新的接受者的数据，且是对面成员的
  get_latest_chat_send(userid){
    let sqlStr = `
    select a.* ,name as receive_user_name,avatar as receive_user_avatar
    from bs_chat a inner join 
    (select max(send_time) as send_time from bs_chat group by room_id) b on a.send_time = b.send_time 
    inner join bs_user on bs_user.id = a.receive_userid 
    where a.send_userid = ${userid} 
    group by chat_id order by a.send_time DESC
    ` 
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  query_chat_room(user1,user2){
    let sqlStr = `
    select room_id
    from bs_chat_room 
    where (user_id_1 = ${user1} and user_id_2 = ${user2})  or 
    (user_id_1 = ${user2} and user_id_2 = ${user1})
  `
  console.log(sqlStr)
  return mysql.query(sqlStr);
  }
  create_chat_room(send_userid,receive_userid,nowtime) {
    let sqlStr = `
    insert into bs_chat_room (user_id_1,user_id_2,update_time) 
    value(`+send_userid+","+receive_userid+",'"+nowtime+"')"
     
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  remark_room(room_id){
    let sqlStr = "update bs_chat set remark=1 where remark=0 and room_id = "+room_id
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  clear_chat_send(user_id_1,user_id_2){
    let sqlStr = `
    update bs_chat set send_userid_delete = 1
    where (send_userid =${user_id_1} and receive_userid =${user_id_2}) 
    `
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  clear_chat_receive(user_id_1,user_id_2){
    let sqlStr = `
    update bs_chat set receive_userid_delete = 1
    where (send_userid =${user_id_2} and receive_userid =${user_id_1})
    `
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
 
}
module.exports = new chat()