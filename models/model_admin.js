/*
 * @Author: yenible
 * @Date: 2021-03-30 21:32:45
 * @LastEditors: yenible
 * @LastEditTime: 2021-05-04 10:43:17
 * @Description: xxx
 */

// 引用mysql文件连接数据库
const mysql = require('../lib/mysql_connect');
// const gravatar = require('gravatar');
const tool = require('../lib/util/tool')

class admin {
  constructor() {}

  login(admin_name,admin_password) {
    let sqlStr = `
      select admin_id,admin_name,avatar 
      from bs_admin 
      where admin_name='`+admin_name+`' and
      admin_password ='`+admin_password+`'
    `
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  get_user_list(email,name,pageno,pagesize){
    let sqlStr = `
    select id,name,email,score,article_amount,file_amount
    from bs_user 
    where name  like '%`+name+`%'   and
    email  like '%`+email+`%' 
    limit 
    ` + pageno * pagesize + ', ' + pagesize
  console.log(sqlStr)
  return mysql.query(sqlStr);
  }
  get_user_list_total(email,name){
    let sqlStr = `
    select count(*)
    from bs_user 
    where name  like '%`+name+`%'   and
    email  like '%`+email+`%' 
    `  
  console.log(sqlStr)
  return mysql.query(sqlStr);
  }
  update_user(dataObj,id){
    let sqlStr = 'update bs_user set '

    Object.keys(dataObj).forEach((item, index) => {
      if (dataObj[item]) {
        // 检测当前的字段类型是否是INT
        if (item.substring(0, 4) === 'INT_') {
         sqlStr += " " + item.substring(4) + "="+ dataObj[item]+ ", ";
        } else {
          sqlStr += " " + item + "='"+ dataObj[item] +"',"
        }
      }
    })
    sqlStr = sqlStr.substring(0,sqlStr.length-2)
    sqlStr += " where id = "+id
  console.log(sqlStr)
  return mysql.query(sqlStr);
  }
  delete_user(id){
    let sqlStr = "delete from bs_user where id = " + id;
  console.log(sqlStr)
  return mysql.query(sqlStr);
  }
  search_blog_admin(authName,title,text, pagesize, pageno){
    let sqlStr = `
    select name,bs_article.article_id,article_content,article_title,user_id,article_type,like_amount,bower_amount,article_collect_amount,article_state,update_time,publish_type
    from bs_article inner join bs_user on id=user_id
    where
    publish_type  = 1  and 
    article_title like  '%` + title + `%' and 
 article_content like  '%` + text + `%'  and
 name like  '%` + authName + `%' 
 limit  
 ` + pageno * pagesize + ', ' + pagesize
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  search_blog_admin_total(authName,title,text){
    let sqlStr = `
    select  count(*)
    from bs_article inner join bs_user on id=user_id
    where
    publish_type  = 1  and 
    article_title like  '%` + title + `%' and 
 article_content like  '%` + text + `%'  and
 name like  '%` + authName + `%' 
`
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  search_file_admin(authName,title,text, pagesize, pageno){
    let sqlStr = `
    select file_desc,bower_amount,collect_amount,file_id,file_score,file_title,like_amount,upload_time,user_id,name 
    from bs_file inner join bs_user on id=user_id
    where
    file_title like  '%` + title + `%' and 
    file_desc like  '%` + text + `%'  and
 name like  '%` + authName + `%' 
 order by upload_time DESC
 limit  
 ` + pageno * pagesize + ', ' + pagesize
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  search_file_admin_total(authName,title,text){
    let sqlStr = `
    select  count(*)
    from bs_file inner join bs_user on id=user_id
    where
    file_title like  '%` + title + `%' and 
    file_desc like  '%` + text + `%'  and
 name like  '%` + authName + `%' 
`
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  get_suggest(title,content,suggest_state, pagesize, pageno){
    let sqlStr = `
    select  suggest_message.*,name
    from suggest_message inner join bs_user on id=user_id
    where
    suggest_content like  '%` + content + `%' and 
    suggest_title like  '%` + title + `%'  and
    suggest_state =`+suggest_state+` 
    order by create_time
    limit ${pageno*pagesize},${pagesize}
`
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  get_suggest_total(title,content,suggest_state, pagesize, pageno){
    let sqlStr = `
    select  count(*)
    from suggest_message inner join bs_user on id=user_id
    where
    suggest_content like  '%` + content + `%' and 
    suggest_title like  '%` + title + `%'  and
    suggest_state =`+suggest_state+` 
`
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  reply_suggest(suggest_id,content,admin_id,reply_time){
    let sqlStr = `update suggest_message 
    set reply_content='${content}' ,reply_admin='${admin_id}' ,reply_time='${reply_time}',suggest_state=1
    where suggest_id =${suggest_id} `

    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  get_admin_list(){
    let sqlStr = `
      select admin_name,level,admin_id 
      from bs_admin 
    `

    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  create_admin(name,password,level,salt){
    let sqlStr = `
    insert into bs_admin (admin_name,admin_password,level,salt)
    value ('${name}','${password}',${level},'${salt}')
    `
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  query_admin_name(name){
    let sqlStr = `
    select admin_name,salt
      from bs_admin where admin_name='${name}'
    `
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  delete_admin(id){
    let sqlStr = "delete from bs_admin where admin_id = " + id;

    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  update_admin_password(name,password){
    let sqlStr = `update bs_admin set admin_password = '${password}'
    where admin_name='${name}'`

    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
}

// console.log(new user("ye", 123, "944329384@qq.com", 555).register())
module.exports = new admin()