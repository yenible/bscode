/*
 * @Author: yenible
 * @Date: 2021-03-14 23:07:39
 * @LastEditors: yenible
 * @LastEditTime: 2021-04-07 19:34:15
 * @Description: xxx
 */
/*
 * @Author: yenible
 * @Date: 2021-03-12 20:53:36
 * @LastEditors: yenible
 * @LastEditTime: 2021-03-12 23:27:05
 * @Description: xxx
 */

// 引用mysql文件连接数据库
const mysql = require('../lib/mysql_connect');
// const gravatar = require('gravatar');
const tool = require('../lib/util/tool')

class file {
  constructor() {}
  save_file(dataObj) {
    // 拼接sql语句
    let preStr = "";
    let nextStr = "";
    Object.keys(dataObj).forEach(item => {

      if (item.substring(0, 4) === 'INT_') {
        preStr += item.substring(4) + ",";
        nextStr += dataObj[item] + ","
      } else {

        preStr += item + ",";
        nextStr += "'" + dataObj[item] + "',";
      }
    });
    let sqlStr = "insert into bs_file (" + preStr.substring(0, preStr.length - 1) + ") value (" + nextStr.substring(0, nextStr.length - 1) + ")"
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }

  // 查询用户所有上传的文件资源
  query_file_list(user_id, params, pageno, pagesize) {
    let sqlStr = "select "
    params.forEach(item => {
      sqlStr += item + ","
    })
    sqlStr = sqlStr.substring(0, sqlStr.length - 1) + ",name from bs_file,bs_user where user_id = id and user_id = " + user_id + " order by upload_time DESC  ";
    sqlStr += ' limit ' + pageno * pagesize + ', ' + pagesize
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  query_file_list_total(user_id) {
    let sqlStr = "select count(*) from bs_file,bs_user where user_id = id and user_id = " + user_id;
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }

  query_file_edit(file_id, params, user_id) {
    let sqlStr = "select "
    console.log(file_id)
    console.log(user_id)
    params.forEach(item => {
      sqlStr += item + ","
    })
    if (user_id) {
      sqlStr = sqlStr.substring(0, sqlStr.length - 1) + " from bs_file where user_id = " + user_id + ' and file_id=' + file_id + " order by upload_time DESC  ";
    } else {
      sqlStr = sqlStr.substring(0, sqlStr.length - 1) + " from bs_file where file_id=" + file_id + " order by upload_time DESC  ";
    }
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  update_file_info(dataObj, params) {
    let sqlStr = 'update bs_file set '
    Object.keys(dataObj).forEach(item => {
      if (item.substring(0, 4) === 'INT_') {
        sqlStr += item.substring(4) + "=" + dataObj[item] + ",";
      } else {
        sqlStr += item + "=" + "\"" + dataObj[item] + "\",";
      }
    })
    sqlStr = sqlStr.substring(0, sqlStr.length - 1) + "  where ";
    Object.keys(params).forEach(item => {
      if (item.substring(0, 4) === 'INT_') {
        sqlStr += "  " + item.substring(4) + "=" + params[item] + " and";
      } else {
        sqlStr += "  " + item + "=" + "'" + params[item] + "' and";
      }
    })
    sqlStr = sqlStr.substring(0, sqlStr.length - 3) + "   ";
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  delete_file(file_id) {
    let sqlStr = "delete from bs_file where file_id = " + file_id;
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  get_file_detail(file_id) {
    let sqlStr = "select file_id,user_id,file_title,file_desc,file_src,like_amount,upload_time,bs_file.collect_amount,bs_file.bower_amount,name,file_score from bs_file,bs_user where file_id = " + file_id + " and id=user_id";
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }

  // 点赞的接口操作
  create_like(user_id, file_id) {
    let sqlStr = 'insert into bs_user_file_like (user_id,file_id) value (' + user_id + ", " + file_id + ")"
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 2、删除bs_user_article_like的数据，取消点赞
  delete_like(user_id, file_id) {
    let sqlStr = 'delete from bs_user_file_like where user_id =' + user_id + " and file_id = " + file_id
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 3、更新文章的点赞数量
  update_like(file_id, is_like) {
    let sqlStr = '';
    if (is_like === 'true') {
      sqlStr = "update bs_file set like_amount=like_amount+1 where file_id = " + file_id
    } else {
      sqlStr = "update bs_file set like_amount=like_amount-1 where file_id = " + file_id
    }
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 查询当前文章该用户是否有点赞
  query_like(user_id, file_id) {
    let sqlStr = "select user_id,file_id  from bs_user_file_like where file_id = " + file_id + " and user_id =" + user_id
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 查询点赞和收藏的数量
  query_like_collect_amount(file_id) {
    let sqlStr = "select file_id,like_amount,collect_amount  from bs_file where file_id = " + file_id
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 收藏接口操作
  // 1、创建收藏的关联数据
  create_collect(user_id, file_id, time) {
    let sqlStr = 'insert into bs_user_file_collect (user_id,file_id,create_time) value (' + user_id + ", " + file_id + ",'" + time + "')"
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 2、删除bs_user_article_collect的数据，取消收藏
  delete_collect(user_id, file_id) {
    let sqlStr = 'delete from bs_user_file_collect where user_id =' + user_id + " and file_id = " + file_id
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 3、更新文章的点赞数量
  update_collect(file_id, is_like) {
    let sqlStr = '';
    if (is_like === 'true') {
      sqlStr = "update bs_file set collect_amount=collect_amount+1 where file_id = " + file_id
    } else {
      sqlStr = "update bs_file set collect_amount=collect_amount-1 where file_id = " + file_id
    }
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 查询当前文章该用户是否有点赞
  query_collect(user_id, file_id) {
    let sqlStr = "select user_id,file_id  from bs_user_file_collect where file_id = " + file_id + " and user_id =" + user_id
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 查询下载任务是否存在
  query_download_task(user_id, file_id) {
    let sqlStr = 'select task_id,create_time from bs_download_list where user_id=' + user_id + ' and file_id=' + file_id
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  update_download_task_state(task_id, time, end_time) {
    let sqlStr = "update bs_download_list set  end_time='" + end_time + "', create_time='" + time + "'"
    sqlStr += " where task_id = " + task_id
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 新建导出任务
  create_download_task(user_id, file_id, create_time, end_time) {
    let sqlStr = 'insert into bs_download_list (user_id,file_id,create_time,end_time) value (' + user_id + ", " + file_id + ",'" + create_time + "','" + end_time + "')"
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  query_download_list(user_id, pagesize, pageno) {
    // let sqlStr = 'select bs_file.file_id,bs_download_list.user_id,bs_file.upload_time,bs_download_list.create_time,name,task_id,file_title,file_name from bs_file,bs_download_list,bs_user where bs_download_list.file_id=bs_file.file_id and bs_user.id=bs_download_list.user_id and bs_download_list.user_id='+user_id
    let sqlStr = `
    select bs_file.file_id,bs_download_list.user_id,bs_file.upload_time,
    bs_download_list.create_time,name,task_id,file_title,file_name ,bs_download_list.end_time,
    CASE
    WHEN bs_download_list.end_time <date_format(now() ,'%Y-%m-%d %H:%i:%S') THEN
    'invalid'
    ELSE
    'effective'
    END 'isEffective'
    from yenible_bs.bs_file,yenible_bs.bs_download_list,yenible_bs.bs_user 
    where bs_download_list.file_id=bs_file.file_id 
    and bs_user.id=bs_download_list.user_id 
    and bs_download_list.user_id=` + user_id + `
    order by end_time DESC  limit  
    ` + pageno * pagesize + ', ' + pagesize
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  query_download_list_total(user_id) {
    let sqlStr = 'select count(bs_download_list.file_id) from bs_download_list' + ` where bs_download_list.user_id=` + user_id
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  add_file_bower(file_id) {
    let sqlStr = "update bs_file set bower_amount=bower_amount+1 where file_id = " + file_id;
    console.log(sqlStr);
    return mysql.query(sqlStr)
  }
  get_file_sort(sortStr, params, pageno, pagesize) {
    let sqlStr = "select "
    params.forEach(item => {
      sqlStr += item + ","
    })
    if (sortStr === 'new') {
      sqlStr = sqlStr.substring(0, sqlStr.length - 1) + ",name from bs_file,bs_user where user_id = id  order by upload_time DESC  ";
    } else {
      sqlStr = sqlStr.substring(0, sqlStr.length - 1) + ",name from bs_file,bs_user where user_id = id  order by like_amount DESC  ";
    }
    sqlStr += ' limit ' + pageno * pagesize + ', ' + pagesize

    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  get_file_total() {
    let sqlStr = 'select count(*) from bs_file'
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  query_collect_file(user_id, pageno, pagesize) {
    let sqlStr = `
    select bs_file.file_id ,bs_user_file_collect.create_time,file_desc,bower_amount,collect_amount,bs_file.file_id,file_score,file_title,like_amount,upload_time,bs_file.user_id,name 
from (bs_user_file_collect inner  join bs_file on bs_file.file_id = bs_user_file_collect.file_id)
inner  join bs_user on bs_user.id = bs_user_file_collect.user_id 

where   bs_user_file_collect.user_id = ` + user_id + `
order by bs_user_file_collect.create_time DESC limit 
    ` + pageno * pagesize + ', ' + pagesize
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  query_collect_file_total(user_id) {
    let sqlStr = `
    select count(*)
from (bs_user_file_collect inner  join bs_file on bs_file.file_id = bs_user_file_collect.file_id)
inner  join bs_user on bs_user.id = bs_user_file_collect.user_id 
where   bs_user_file_collect.user_id = ` + user_id + `
order by bs_user_file_collect.create_time DESC `
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 上传文件后更新用户表的file_amount
  update_user_file_amount(userid) {
    let sqlStr = 'update bs_user set file_amount=file_amount+1 where id='+userid

    console.log(sqlStr)
    return mysql.query(sqlStr);

  }
    // 保存点赞消息
    save_message_file(file_id,auth_id,user_id,time){
      let sqlStr = 'insert into bs_like_message (user_id,file_id,reply_id,reply_time,reply_type) value (' + auth_id + ", '" + file_id + "',"+user_id+",'"+time+"','file')"
      console.log(sqlStr)
      return mysql.query(sqlStr);
    }
    // 查询点赞消息信息
    query_message_file(file_id,user_id){
      let sqlStr =  "select message_id from bs_like_message where file_id='"+file_id+"' and reply_id = "+user_id
      console.log(sqlStr)
      return mysql.query(sqlStr);
    }
    update_message_file(message_id,time){
      let sqlStr = "update bs_like_message set reply_time='"+time+"' , is_see = 0 where message_id="+message_id
      console.log(sqlStr)
      return mysql.query(sqlStr);
    }
}

module.exports = new file()