/*
 * @Author: yenible
 * @Date: 2021-03-12 20:53:36
 * @LastEditors: yenible
 * @LastEditTime: 2021-04-20 20:24:53
 * @Description: xxx
 */

// 引用mysql文件连接数据库
const mysql = require('../lib/mysql_connect');
// const gravatar = require('gravatar');
const tool = require('../lib/util/tool')

class message {
  constructor() {}

  get_article_file_message(user_id) {
    let sqlStr = `
    select bs_article_file_message.*,name
      from (bs_article_file_message inner join bs_user on id = bs_article_file_message.reply_id)
      where bs_article_file_message.user_id = ` + user_id + `
      order by reply_time DESC`
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  get_article_file_message_total(user_id) {
    let sqlStr = `
    select count(*)
      from (bs_article_file_message inner join bs_user on id = bs_article_file_message.reply_id)
      where bs_article_file_message.user_id = ` + user_id + ` and is_see=0`
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  // 把全部的消息标为已读
  article_file_message_saw(user_id) {
    let sqlStr = "update bs_article_file_message set is_see=1 where is_see=0 and user_id = "+user_id
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  delete_comment_message(message_id) {
    let sqlStr = " delete from bs_article_file_message WHERE message_id = " + message_id
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  get_like_message_article(user_id) {
    let sqlStr = `
    select bs_like_message.*,name,article_title
     from (bs_like_message inner join bs_user on id = bs_like_message.reply_id) inner join bs_article on bs_like_message.article_id = bs_article.article_id
      where bs_like_message.user_id = ` + user_id
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  get_like_message_file(user_id) {

    let sqlStr = `
    select bs_like_message.*,name,file_title
     from (bs_like_message inner join bs_user on id = bs_like_message.reply_id) inner join bs_file on bs_like_message.file_id = bs_file.file_id
      where bs_like_message.user_id = ` + user_id
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  get_like_message_total(user_id) {
    let sqlStr = `
    select count(*)
      from bs_like_message 
      where bs_like_message.user_id = ` + user_id + ` and is_see=0`
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  like_message_saw(user_id) {
    let sqlStr = "update bs_like_message set is_see=1 where is_see=0 and user_id = "+user_id
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  get_message_attract(user_id) {
    let sqlStr = `
    select bs_attract_message.*,name
      from (bs_attract_message inner join bs_user on id = bs_attract_message.reply_id)
      where bs_attract_message.user_id = ` + user_id
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  get_attract_message_total(user_id) {
    let sqlStr = `
    select count(*)
      from bs_attract_message 
      where bs_attract_message.user_id = ` + user_id + ` and is_see=0`
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  attract_message_saw(user_id) {
    let sqlStr = "update bs_attract_message set is_see=1 where is_see=0 and user_id = "+user_id
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  delete_attract_message(message_id) {
    let sqlStr = " delete from bs_attract_message WHERE message_id = " + message_id

    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  delete_like_message(message_id) {
    let sqlStr = " delete from bs_like_message WHERE message_id = " + message_id

    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  submit_suggest(user_id,content,title,create_time){
    let sqlStr = "insert into suggest_message (user_id,suggest_content,suggest_title,create_time) value ("+user_id+",'"+content+"','"+title+"','"+create_time+"')"
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  get_user_suggest(user_id,suggest_state,pageno,pagesize){
    let sqlStr = `
    select * 
    from suggest_message
    where user_id = ${user_id} and  suggest_state=${suggest_state} 
    order by reply_time DESC
    limit ${pageno*pagesize},${pagesize}
    `
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
  get_user_suggest_total(user_id,suggest_state){
    let sqlStr = `
    select count(*) 
    from suggest_message
    where user_id = ${user_id} and  suggest_state=${suggest_state} 

    `
    console.log(sqlStr)
    return mysql.query(sqlStr);
  }
}
module.exports = new message()