/*
 * @Author: yenible
 * @Date: 2020-12-14 22:55:24
 * @LastEditors: yenible
 * @LastEditTime: 2021-03-22 09:58:28
 * @Description: user model
 */
// 引用mysql文件连接数据库
const mysql = require('../lib/mysql_connect');
// const gravatar = require('gravatar');
const tool = require('../lib/util/tool')

// 当前模型允许操作的表
const table = 'bs_user';

class user {
    constructor() {}
    // 注册账号
    register(dataObj) {
        // let nowTime = new Date().Format('yyyy-MM-dd HH:mm:ss');//毫秒级别的
        let nowTime = tool.format(new Date()); //毫秒级别的
        console.log(nowTime);
        // 拼接sql语句
        let sqlStr = "insert into " + table + " (" + Object.keys(dataObj).join(", ") + ", create_time, login_time) value ('" + Object.values(dataObj).join("', '") + "', '" + nowTime + "', '" + nowTime + "') "
        console.log(sqlStr)
        return mysql.query(sqlStr);
    }
    // 根据给定的参数来查询对应的用户数据,resParams为需要返回的字段（数组）
    find_user(params, resParams) {
        let keys = Object.keys(params);
        let fieldStr = "";
        if ((!Array.isArray(resParams)) || resParams.length <= 0) {
            fieldStr = " * ";
        } else {
            fieldStr = resParams.join(', ')
        }
        // 注意有的类型是字符串有的不是字符串要进行区分加不加单引号       
        let sqlStr = "select " + fieldStr + " from " + table;
        if (keys.length > 0) {
            let str = "";
            keys.forEach(res => {
                if (str !== "") {
                    str += " and ";
                }
                // 检测当前的字段类型是否是INT
                if (res.substring(0, 4) === 'INT_') {
                    str += res.substring(4) + " = " + params[res] + " "
                } else {
                    str += res + " = '" + params[res] + "' "
                }
            })
            sqlStr = sqlStr + " where " + str;
            // console.log(sqlStr)
        }
        console.log(sqlStr)
        return mysql.query(sqlStr);
    }
    // 更新用户信息
    update_user(updateObj) {
        let sqlStr = 'update ' + table + ' set '
        for (let key in updateObj) {
            if (key == 'id') {
                console.log(key)
                continue;
            }
            if (sqlStr == 'update ' + table + ' set ') {
                sqlStr += key + " = '" + updateObj[key] + "'"
            } else {
                sqlStr += ", " + key + " = '" + updateObj[key] + "'"
            }
        }
        // 加上条件
        sqlStr += " where id =" + updateObj.id
        // console.log(sqlStr)
        return mysql.query(sqlStr);

    }
    add_avatar(user_id, imgUrl) {
        let sqlStr = " update bs_user set avatar = '" + imgUrl + "' where id = " + user_id
         console.log(sqlStr)
         return mysql.query(sqlStr);
    }
    update_score(user_id,score,action){
        let sqlStr = 'update bs_user set score = '
        if(action==='add'){
            sqlStr += 'score + '+ score
        }else if(action ==='reduce'){
            sqlStr += 'score - '+ score
        }
        sqlStr += ' where id = ' + user_id;
        console.log(sqlStr)
         return mysql.query(sqlStr);
    }
}
// console.log(new user("ye", 123, "944329384@qq.com", 555).register())
module.exports = new user()