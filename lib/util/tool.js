/*
 * @Author: yenible
 * @Date: 2021-01-12 17:01:39
 * @LastEditors: yenible
 * @LastEditTime: 2021-04-18 13:42:12
 * @Description: xxx
 */
const jwt = require('jsonwebtoken'); //token生成插件

module.exports = tool = {
  checkToken:function(ctx) {
    //   console.log(ctx)
    if(!ctx.request.header.authorization){
        return {
            body: {
                errCode: 1,
                msg: "登录已过期，233请重新登录",
                redirect: '/login',
                errObj: 'login'
            },
            code: 1
        }
    }
    let tokenInfo = ctx.request.header.authorization.split(" ");
    if ( tokenInfo[0]!=='null' && tokenInfo.length == 2) {
  
        let tokenDecode = jwt.decode(tokenInfo[0], {
            complete: true
        })
        let date = new Date();
        // console.log(date);
        // console.log(date / 1000);
        // 校验时间戳是否过期
        if (tokenDecode.payload.exp < date / 1000) {
            // 时间戳过期
            return {
                body: {
                    errCode: 1,
                    msg: "登录已过期，000请重新登录",
                    redirect: '/login',
                    errObj: 'login'
                },
                code: 1
            }
  
        } else {
            // 检验tokenDecode的name和tokenName是否是同一个（防止有人手动修改localstorage、）
            // console.log(tokenInfo)
            // console.log(tokenInfo[1])
            // console.log(tokenDecode.payload.data.id)
            if (tokenInfo[1] != tokenDecode.payload.data.id) {
                return {
                    body: {
                        errCode: 1,
                        msg: "登录已过期，111请重新登录",
                        redirect: '/login',
                        errObj: 'login'
                    },
                    code: 1
                }
            } else {
                return {
                    code: 0,
                    id:tokenDecode.payload.data.id,
                    isAdmin:tokenDecode.payload.data.isAdmin
                }
            }
        }
        // await next();
  
    } else {
        // console.log(ctx.request.header);
        return {
            body: {
                errCode: 1,
                msg: "登录已过期，222请重新登录",
                redirect: '/login',
                errObj: 'login'
            },
            code: 1
        }
        // await next();
    }
  },
  'test': function(){
    console.log(123)
  },
  format:function(Date){
    var Y = Date.getFullYear();
    var M = Date.getMonth() + 1;
        M = M < 10 ? '0' + M : M;// 不够两位补充0
    var D = Date.getDate();
        D = D < 10 ? '0' + D : D;
    var H = Date.getHours();
        H = H < 10 ? '0' + H : H;
    var Mi = Date.getMinutes();
        Mi = Mi < 10 ? '0' + Mi : Mi;
    var S = Date.getSeconds();
        S = S < 10 ? '0' + S : S;
        return Y + '-' + M + '-' + D + ' ' + H + ':' + Mi + ':' + S;
}
}
  