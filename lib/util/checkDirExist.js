/*
 * @Author: yenible
 * @Date: 2021-01-26 17:19:08
 * @LastEditors: yenible
 * @LastEditTime: 2021-01-26 17:19:08
 * @Description: 判断文件夹是否存在 如果不存在则创建文件夹
 */
 
const path = require('path');
const fs = require('fs');

function checkDirExist(p) {
  if (!fs.existsSync(p)) {
    fs.mkdirSync(p);
  }
}

module.exports = checkDirExist;