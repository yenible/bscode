/*
 * @Author: yenible
 * @Date: 2021-02-21 19:43:59
 * @LastEditors: yenible
 * @LastEditTime: 2021-02-21 20:34:34
 * @Description: xxx
 */
const path = require('path');
const fs = require('fs');
let stringVary = {
   in(str) {
    let newStr = str.replace(/\"/g,"&#34;");//双引号
    newStr =newStr.replace(/\&/g,"&#38;");//双引号
    newStr =newStr.replace(/\</g,"&#60;");//双引号
    newStr =newStr.replace(/\>/g,"&#62;");//双引号
    //  console.log(newStr)
     return newStr;
  },
  out(str){
    let newStr = str.replace(/&#34;/g,"\"");//双引号
    newStr =newStr.replace(/&#38;/g,"&");
    newStr =newStr.replace(/&#60;/g,"<");  
    newStr =newStr.replace(/&#62;/g,">");
    // console.log(newStr)
     return newStr;  
  }
}
 

module.exports = stringVary;